import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import { createProvider } from "./vue-apollo";
import vuetify from "./plugins/vuetify";
import * as Sentry from "@sentry/browser";
import * as Integrations from "@sentry/integrations";

Sentry.init({
  dsn:
    "https://a32a118a30274cadb791e729d2ac30ca@o369102.ingest.sentry.io/5176995",
  integrations: [
    new Integrations.Vue({ Vue, attachProps: true, logErrors: false })
  ]
});

Vue.config.productionTip = false;

new Vue({
  router,
  apolloProvider: createProvider(),
  vuetify,
  render: h => h(App)
}).$mount("#app");
