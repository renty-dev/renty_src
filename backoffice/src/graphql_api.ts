interface Login {
  token: string;
  validity?: number;
  createdAt?: string;
  type?: string;
  otp?: boolean;
}

interface Notify {
  sent: number;
  devices: [string] | null;
}

interface LoginData {
  login: Login;
}

interface NotifyData {
  notify: Notify;
}
type Checking = LoginData | NotifyData;

function islogindata(toBeDetermined: Checking): toBeDetermined is LoginData {
  return (toBeDetermined as LoginData).login ? true : false;
}

function isnotifydata(toBeDetermined: Checking): toBeDetermined is NotifyData {
  return (toBeDetermined as NotifyData).notify ? true : false;
}

interface Data extends Response {
  data: LoginData | NotifyData;
  error?: object;
}

class RentyGraphQLAPI {
  private static instance: RentyGraphQLAPI;
  private _gqlendpoint: string;
  private _authoken: string | null;

  private constructor() {
    console.log("New RentyGraphAPI instance created");
    this._gqlendpoint = "http://localhost:4000/query";
    this._authoken = localStorage.getItem("gql-token");
  }

  public static getInstance(): RentyGraphQLAPI {
    if (!RentyGraphQLAPI.instance) {
      return new RentyGraphQLAPI();
    }
    return this.instance;
  }
  public async login(
    username: string,
    password: string
  ): Promise<string | null> {
    let rt = null;
    const res: Response = await fetch(this._gqlendpoint, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        query: `query { 
            login ( input: { name: "${username}" password: "${password}" } ) { 
                token 
            }
        }`
      })
    });
    const data: Data = await res.json();
    if (data.error != null) {
      rt = null;
    } else if (islogindata(data.data)) {
      rt = data.data.login.token;
      this._authoken = data.data.login.token;
    }
    return rt;
  }

  public async notify(
    username: string,
    title: string,
    body: string,
    preferedTarget?: string
  ): Promise<Notify | null> {
    let rt = null;
    if (this._authoken == null) return rt;
    let inputs = `
        username: "${username}" 
        title: "${title}"
        body:  "${body}"
    `;
    if (preferedTarget) inputs += `preferedTarget: ${preferedTarget}`;
    const res: Response = await fetch(this._gqlendpoint, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: this._authoken
      },
      body: JSON.stringify({
        query: `query { 
            notify ( input: { ${inputs} } ) { 
                sent
                devices 
            }
        }`
      })
    });
    const data: Data = await res.json();
    if (data.error != null) {
      rt = null;
    } else if (isnotifydata(data.data)) {
      rt = data.data.notify;
    }
    return rt;
  }
}

export default RentyGraphQLAPI;
