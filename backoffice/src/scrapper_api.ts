interface ScrapperCmd {
  authorization: string;
  command: string;
}

interface ScrapperMessage {
  message?: string;
  result?: string;
  error?: string;
}

class ScrapperAPI {
  private static instance: ScrapperAPI;
  private _scrapperendpoint: string;
  private _socket: WebSocket;
  private _authoken: string | null;

  private constructor() {
    console.log("New ScrapperAPI instance created");
    this._scrapperendpoint = "ws://localhost:5000/scrapper/live";
    this._socket = new WebSocket(this._scrapperendpoint);
    this._authoken = localStorage.getItem("gql-token");
  }
  public static getInstance(): ScrapperAPI {
    if (!ScrapperAPI.instance) {
      return new ScrapperAPI();
    }
    return this.instance;
  }
  public getSocket(): WebSocket {
    return this._socket;
  }

  public scrapp(cmd: ScrapperCmd) {
    this._socket.send(JSON.stringify(cmd));
  }
}

export default ScrapperAPI;
