image: docker:19.03.1
#   __   ___      ___         __
#  |__) |__  |\ |  |  \ /    /  ` |
#  |  \ |___ | \|  |   |     \__, |
#
####### © Renty 2020 - All rights reserved
services:
  - name: docker:19.03.1-dind

##
# From https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#using-docker-caching
variables:
  ## DOCKER_DRIVER: overlay2 <- Set in runner.conf
  ## Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"

stages:
  - scan
  - test
  - build_backend
  - build_frontend
  - deploy_backend_staging
  - deploy_frontend_staging
  - uat_backend
  - uat_frontend
  - uat_mobile

include:
  - template: Container-Scanning.gitlab-ci.yml
  
#### SCAN
### - Static analysis tools
scan_mobile:
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|audit|mobile|staging|master)/
  stage: scan
  variables:
    IMAGE_TAG: cirrusci/flutter:dev
  script:
    - cd mobile/renty
    - docker run --rm -v ${PWD}:/build --workdir /build $IMAGE_TAG flutter analyze | tee result.txt && echo "👍 So far so good" || exit 1
    - echo "At $(date) Well done mobile code is clean ! 💎" | tee -a result.txt
  artifacts:
    paths:
      - mobile/renty/result.txt
    expire_in: 1 week

scan_backend:
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|audit|backend|staging|master)/
  stage: scan
  variables:
    IMAGE_TAG: codeclimate/codeclimate
  services:
    - docker:19.03.1-dind
  before_script:
    - apk update && apk add bash make git go yarn
    - yarn global add prisma
    - wget -O - -q https://raw.githubusercontent.com/securego/gosec/master/install.sh | sh -s v2.2.0
    - cd backend && make codegen && cd -
    - export PATH=$PATH:$(pwd)/bin
    - docker pull $IMAGE_TAG
  script:
    - cd backend/graphql_server
    - make audit
    - make sast
    - cd ../../
    - cd backend/scrapper
    - make audit
    - cd ../../
    - cd backend/geoip
    - make sast
  when: on_success
  artifacts:
    paths:
      - backend/graphql_server/reports/
      - backend/scrapper/reports/
      - backend/geoip/reports/
    expire_in: 2 week

#### TEST
### - Unit testing (#WIP + #TODO('s))
test_backend:
  image: golang:alpine
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|backend|staging|master)/
  stage: test
  before_script:
    - apk update && apk add make git bash yarn go
    - git fetch --tags # Workaround for https://gitlab.com/gitlab-org/gitaly/issues/2221
    - yarn global add prisma
    - cd backend && make codegen && cd -
  script:
    - cd backend/graphql_server && make test | tee result.txt && echo "Write some test ..." || exit 1 && cd -
    - cd backend/scrapper && go test ./... | tee -a result.txt  && echo "Write some test ..." || exit 1 && echo "Keep going ..." && cd -
  artifacts:
    paths:
      - backend/**/result.txt

#### BUILD
### - Building stack steps
build_graphqlserver:
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|backend|staging|master)/
  stage: build_backend
  variables:
    BUILD_TAG: $CI_REGISTRY_IMAGE/graphqlserver:build-step
    MAIN_TAG: $CI_REGISTRY_IMAGE/graphqlserver:latest
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - apk update && apk add make git bash yarn
    - git fetch --tags # Workaround for https://gitlab.com/gitlab-org/gitaly/issues/2221
    - yarn global add prisma
    - cd backend && sed -i 's@http:\/\/localhost:4466\/renty\/dev@'"$PRISMA_ENDPOINT"'@' prisma.yml
    - make codegen && cd -
    - docker pull $BUILD_TAG || true
    - docker pull $MAIN_TAG || true
  script:
    - cd backend/graphql_server && make image && make pushimage

build_scrapper:
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|backend|staging|master)/
  stage: build_backend
  variables:
    BUILD_TAG: $CI_REGISTRY_IMAGE/scrappy:build-step
    MAIN_TAG: $CI_REGISTRY_IMAGE/scrappy:latest
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - apk update && apk add make git bash yarn
    - git fetch --tags # Workaround for https://gitlab.com/gitlab-org/gitaly/issues/2221
    - yarn global add prisma
    - cd backend && sed -i 's@http:\/\/localhost:4466\/renty\/dev@'"$PRISMA_ENDPOINT"'@' prisma.yml
    - make codegen && cd -
    - docker pull $BUILD_TAG || true
    - docker pull $MAIN_TAG || true
  script:
    - cd backend/scrapper && make image && make pushimage

build_geoip:
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|backend|staging|master)/
  stage: build_backend
  variables:
    BUILD_TAG: $CI_REGISTRY_IMAGE/geoip:build-step
    MAIN_TAG: $CI_REGISTRY_IMAGE/geoip:latest
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - apk update && apk add make git bash yarn git-lfs
    - git fetch --tags
    - git lfs pull
    - docker pull $BUILD_TAG || true
    - docker pull $MAIN_TAG || true
  script:
    - cd backend/geoip
    - ls datas/latest
    - make image
    - make pushimage

build_backoffice:
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|backoffice|backend|staging|master)/
  stage: build_frontend
  variables:
    BUILD_TAG: $CI_REGISTRY_IMAGE/backoffice:build-step
    MAIN_TAG: $CI_REGISTRY_IMAGE/backoffice:latest
  before_script:
    - apk update && apk add make git bash yarn
    - yarn global add prisma
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $BUILD_TAG || true
    - docker pull $MAIN_TAG || true
  script:
    - cd backoffice
    - sed -i 's@http:\/\/localhost:4466\/renty\/dev@'"$PRISMA_ENDPOINT"'@' src/vue-apollo.js
    - sed -i 's@ws:\/\/localhost:4466\/renty\/dev@'"$PRISMA_WSENDPOINT"'@' src/vue-apollo.js
    - sed -i 's@ws:\/\/localhost:4200\/scrapper\/live@'"$SCRAPPER_ENDPOINT"'@' src/components/ScrapperControl.vue
    - make image && make pushimage

build_webapp:
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|front|staging|master)/
  stage: build_frontend
  variables:
    BUILD_TAG: $CI_REGISTRY_IMAGE/renty-frontend:build-step
    MAIN_TAG: $CI_REGISTRY_IMAGE/renty-frontend:latest
  before_script:
    - apk update && apk add make git bash yarn
    - git fetch --tags # Workaround for https://gitlab.com/gitlab-org/gitaly/issues/2221
    - yarn global add prisma
    - cd backend && make codegen && cd -
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $BUILD_TAG || true
    - docker pull $MAIN_TAG || true
  script:
    - export IMAGE_TAG="$CI_REGISTRY_IMAGE/renty-frontend:$(git describe --tags | cut -d '-' -f1)-$(git rev-parse --short HEAD)"
    - sed -i 's@http:\/\/localhost:4000\/query@'"$GQL_ENDPOINT"'@' ./frontend/renty/src/apollo_client.js
    - docker build --target build-stage --cache-from $BUILD_TAG -t $BUILD_TAG ./frontend/renty
    - docker build --cache-from $BUILD_TAG --cache-from $MAIN_TAG -t $MAIN_TAG -t $IMAGE_TAG ./frontend/renty
    - docker push $IMAGE_TAG && docker push $MAIN_TAG

build_mobile:
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|mobile|staging|master)/
  stage: build_frontend
  variables:
    BUILD_TAG: registry.gitlab.com/renty-dev/renty_src/renty-mobile:build-step
    MAIN_TAG: registry.gitlab.com/renty-dev/renty_src/renty-mobile:latest
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - apk update && apk add make git bash yarn
    - git fetch --tags # Workaround for https://gitlab.com/gitlab-org/gitaly/issues/2221
    - apk add --repository https://alpine.secrethub.io/alpine/edge/main --allow-untrusted secrethub-cli # Add secret management
    - git fetch --tags # Workaround for https://gitlab.com/gitlab-org/gitaly/issues/222
    - docker pull $BUILD_TAG || true
    - docker pull $MAIN_TAG || true
  script:
    - export IMAGE_TAG="$CI_REGISTRY_IMAGE/renty-mobile:$(git describe --tags | cut -d '-' -f1)-$(git rev-parse --short HEAD)"
    - secrethub read --out-file ./mobile/renty/android/app/google-services.json Renty/mobile/google-services.json
    - sed -i 's@http:\/\/localhost:4000\/query@'"$GQL_ENDPOINT"'@' ./mobile/renty/lib/utils/config.dart
    - docker build --target build --cache-from $BUILD_TAG -t $BUILD_TAG ./mobile/renty
    - docker build --cache-from $BUILD_TAG --cache-from $MAIN_TAG -t $MAIN_TAG ./mobile/renty
    - export id=$(docker create $MAIN_TAG)
    - docker cp $id:/usr/share/nginx/html/android .
    - docker rm -v $id
    - docker push $BUILD_TAG && docker push $MAIN_TAG
  when: on_success
  artifacts:
    paths:
      - android/
    expire_in: 1 week

#### DEPLOY
### - Dev/Staging : Heroku
### - Master/Production : ??
deploy_database_staging:
  stage: deploy_backend_staging
  variables:
    HEROKU_APP: renty-dev-87c95aef34
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|deploy|staging|master)/
  before_script:
    - apk update && apk add make git bash yarn
    - yarn global add prisma
  script:
    - cd backend/ && make dbscheme || true


deploy_graphqlserver_staging:
  stage: deploy_backend_staging
  variables:
    HEROKU_APP: renty-gqlserver-dev
    MAIN_TAG: $CI_REGISTRY_IMAGE/graphqlserver:latest
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|deploy|staging|master)/
  before_script:
    - apk update && apk add make git bash yarn
    - yarn global add heroku
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $MAIN_TAG || true
    - docker logout
  script:
    - docker login --username=_ --password=$(heroku auth:token) registry.heroku.com
    - export IMAGE_TAG="registry.heroku.com/$HEROKU_APP/web"
    - docker pull $IMAGE_TAG || true
    - docker image tag $MAIN_TAG $IMAGE_TAG
    - docker push $IMAGE_TAG
    - heroku container:release web --app $HEROKU_APP

deploy_geoapi_staging:
  stage: deploy_backend_staging
  variables:
    HEROKU_APP: renty-geoip-dev
    MAIN_TAG: $CI_REGISTRY_IMAGE/geoip:latest
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|deploy|staging|master)/
  before_script:
    - apk update && apk add make git bash yarn
    - yarn global add heroku
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $MAIN_TAG || true
    - docker logout
  script:
    - docker login --username=_ --password=$(heroku auth:token) registry.heroku.com
    - export IMAGE_TAG="registry.heroku.com/$HEROKU_APP/web"
    - docker pull $IMAGE_TAG || true
    - docker image tag $MAIN_TAG $IMAGE_TAG
    - docker push $IMAGE_TAG
    - heroku container:release web --app $HEROKU_APP

deploy_scrapper_staging:
  stage: deploy_backend_staging
  variables:
    HEROKU_APP: renty-scrapper-dev
    MAIN_TAG: $CI_REGISTRY_IMAGE/scrappy:latest
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|deploy|staging|master)/
  before_script:
    - apk update && apk add make git bash yarn
    - yarn global add heroku
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $MAIN_TAG || true
    - docker logout
  script:
    - docker login --username=_ --password=$(heroku auth:token) registry.heroku.com
    - export IMAGE_TAG="registry.heroku.com/$HEROKU_APP/web"
    - docker pull $IMAGE_TAG || true
    - docker image tag $MAIN_TAG $IMAGE_TAG
    - docker push $IMAGE_TAG
    - heroku container:release web --app $HEROKU_APP

deploy_backoffice_staging:
  stage: deploy_frontend_staging
  variables:
    HEROKU_APP: renty-backoffice-dev
    MAIN_TAG: $CI_REGISTRY_IMAGE/backoffice:latest
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|deploy|staging|master)/
  before_script:
    - apk update && apk add make git bash yarn
    - yarn global add heroku
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $MAIN_TAG || true
    - docker logout
  script:
    - docker login --username=_ --password=$(heroku auth:token) registry.heroku.com
    - export IMAGE_TAG="registry.heroku.com/$HEROKU_APP/web"
    - docker pull $IMAGE_TAG || true
    - docker image tag $MAIN_TAG $IMAGE_TAG
    - docker push $IMAGE_TAG
    - heroku container:release web --app $HEROKU_APP

deploy_webapp_staging:
  stage: deploy_frontend_staging
  variables:
    HEROKU_APP: renty-frontend-dev
    MAIN_TAG: $CI_REGISTRY_IMAGE/renty-frontend:latest
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|deploy|staging|master)/
  before_script:
    - apk update && apk add make git bash yarn
    - yarn global add heroku
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $MAIN_TAG || true
    - docker logout
  script:
    - docker login --username=_ --password=$(heroku auth:token) registry.heroku.com
    - export IMAGE_TAG="registry.heroku.com/$HEROKU_APP/web"
    - docker pull $IMAGE_TAG || true
    - docker image tag $MAIN_TAG $IMAGE_TAG
    - docker push $IMAGE_TAG
    - heroku container:release web --app $HEROKU_APP

deploy_mobile_staging:
  stage: deploy_frontend_staging
  variables:
    HEROKU_APP: renty-mobile-dev
    MAIN_TAG: registry.gitlab.com/renty-dev/renty_src/renty-mobile:latest
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|deploy|staging|master)/
  before_script:
    - apk update && apk add make git bash yarn
    - yarn global add heroku
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $MAIN_TAG || true
    - docker logout
  script:
    - docker login --username=_ --password=$(heroku auth:token) registry.heroku.com
    - export IMAGE_TAG="registry.heroku.com/$HEROKU_APP/web"
    - docker pull $IMAGE_TAG || true
    - docker image tag $MAIN_TAG $IMAGE_TAG
    - docker push $IMAGE_TAG
    - heroku container:release web --app $HEROKU_APP

#### User Acceptance Test
###
acceptancetest_mobile:
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /(ci|mobile|staging|master)/
  stage: uat_mobile
  variables:
    IMAGE_TAG: registry.gitlab.com/renty-dev/devtools/android-screenshots:latest
  services:
    - docker:19.03.1-dind
  before_script:
    - docker login -u $SCANNER_REGISTRY_USER -p $SCANNER_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - mkdir -p screenshots/logs && ls
    - docker run --privileged -v "$(pwd)/android/app-release.apk:/app/app-release.apk" -v "$(pwd)/screenshots:/app/screenshots" $IMAGE_TAG
    - ls && echo "ok"
  artifacts:
    paths:
      - screenshots/
    expire_in: 1 week
