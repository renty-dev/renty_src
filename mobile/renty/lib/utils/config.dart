class Config {
  // TIPS: use `adb reverse tcp:4000 tcp:4000` to bind localhost from host to emulator/mobile
  static String get graphQLEndpoint => "http://localhost:4000/query";
  static String get authTokenPrefKey => "token";
  static String get sentryDSN => "https://e4f79170a12f462480d5a85b326d3b67@sentry.io/5177017";
}
