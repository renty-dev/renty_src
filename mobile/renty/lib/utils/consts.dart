// lib/login.dart
/// => `Welcome back [name] !`
String WelcomeMessage(String name) => "Welcome back $name 👋";

///  => `Please verify your informations.`
String BadLogin = "Please verify your informations.";

/// => `Name`
String NameInputHint = "Name";

/// => `Password`
String PasswordInputHint = "Password";
// lib/register.dart
/// => `Succesfully registered [name]`
String RegistrationSucess(String name) => 'Successfully registered ' + name;

///  => `Please verify your informations.`
String BadRegistration = 'Please verify your informations.';

/// => `Name *`
String NameRegisterHint = 'Name *';
String EmailRgisterHint = 'Email';
String PhoneRegisterHint = 'Phone';
String PasswordRegisterHint = 'Password *';
String MandatoryText = '* Indicate required fields';
// lib/main.dart
String AppTitle = '🏘️ Renty';
String AboutButtonLabel = 'About';
String LoginButtonLabel = 'Login';
String LogoutButtonLabel = 'Logout';

/// => `Recover`
String RecoverButtonLabel = 'Recover';

/// => `Register`
String RegisterButtonLabel = 'Register';

/// => `Dashboard`
String DashboardButtonLabel = 'Dashboard';

/// => `Lost password`
String RecoverButtonLabelText = 'Lost password';

/// => `Sign in with Google`
String SigninWithGoogleLabel = 'Sign in with Google';

/// => `Register with Google`
String RegisterWithGoogleLabel = 'Register with Google';
