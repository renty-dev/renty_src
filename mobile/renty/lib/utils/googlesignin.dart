import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:renty/utils/consts.dart';

GoogleSignIn GoogleSignInObject = GoogleSignIn(
  signInOption: SignInOption.standard,
  scopes: <String>['email'],
);

Widget SignInButton(void Function() handler) {
  return OutlineButton(
    splashColor: Colors.grey,
    onPressed: handler,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
    highlightElevation: 0,
    borderSide: BorderSide(color: Colors.blue),
    child: Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image(image: AssetImage("assets/google_logo.png"), height: 35.0),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(
              SigninWithGoogleLabel,
              style: TextStyle(
                fontSize: 20,
                color: Colors.grey,
              ),
            ),
          )
        ],
      ),
    ),
  );
}

Widget GoogleRegisterButton(void Function() handler) {
  return OutlineButton(
    splashColor: Colors.grey,
    onPressed: handler,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
    highlightElevation: 0,
    borderSide: BorderSide(color: Colors.blue),
    child: Padding(
      padding: const EdgeInsets.all(5),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image(image: AssetImage("assets/google_logo.png"), height: 35.0),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(
              RegisterWithGoogleLabel,
              style: TextStyle(
                fontSize: 20,
                color: Colors.grey,
              ),
            ),
          )
        ],
      ),
    ),
  );
}
