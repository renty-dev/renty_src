class RentyQuery {
  static String login(String name, String password, String notificationID) {
    return """
      query {
        login(input: { 
          name: "$name" 
          password: "$password" 
          notificationID: "$notificationID" 
          }) {
          token
        }
      }   
    """;
  }

  static String sociallogin(String idtoken, String notificationID) {
    return """
      query {
        socialLogin(input: { 
          IDToken: "$idtoken"
          provider: GOOGLE
          notificationID: "$notificationID" 
          }) {
          token
        }
      }   
    """;
  }

  static String user() {
    return """
      query {
        user {
          name
          civility
          firstName
          lastName
          email {
            value
            verified
          }
          phone {
            value
            verified
          }
          createdAt
          updatedAt
        }
      }
    """;
  }

  static String rentoffers() {
    return """
      query {
        offers {
          id
          price {
            value
            curr
          }
          assets {
            url
          }
          location {
            latitude
            longitude
          }
          title {
            text
          }
          createdAt
          updatedAt
        }
      }
    """;
  }
}
