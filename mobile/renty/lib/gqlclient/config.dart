import "package:flutter/material.dart";
import "package:graphql_flutter/graphql_flutter.dart";
import 'package:renty/utils/config.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GraphQLConfiguration {
  // TODO: Set a custom user agent in gql http client
  // final HttpClient httpClient = new HttpClient();
  ValueNotifier<GraphQLClient> client;
  HttpLink httpLink;

  GraphQLConfiguration() {
    // this.httpClient.userAgent = "renty-mobile-dev-v0.1.1";
    AuthLink authLink = AuthLink(getToken: () async {
      var prefs = await SharedPreferences.getInstance();
      var istoken = prefs.get(Config.authTokenPrefKey);
      if (istoken != "") {
        return "Bearer " + istoken;
      }
      return "";
    });
    this.httpLink = HttpLink(
      uri: Config.graphQLEndpoint,
      // httpClient: this.httpClient as Client,
    );
    WidgetsFlutterBinding.ensureInitialized();
    this.client = ValueNotifier(
      GraphQLClient(
        link: authLink.concat(httpLink),
        cache: InMemoryCache(),
      ),
    );
  }

  GraphQLClient clientToQuery() {
    return GraphQLClient(
      cache: InMemoryCache(),
      link: httpLink,
    );
  }
}
