class RentyMutation {
  static String socialregister(String idtoken, String notificationID) {
    return """
    mutation {
      socialregister(
        input: {
           IDToken: "$idtoken"
           provider: GOOGLE
           notificationID: "$notificationID"
        }
      ) {
        name
      }
    }
    """;
  }

  static String register(
      String name, String email, String password, String notificationID) {
    return """
    mutation {
      register(
        input: {
           name: "$name"
           email: "$email"
           password: "$password"
           notificationID: "$notificationID"
        }
      ) {
        name
      }
    }
    """;
  }
}
