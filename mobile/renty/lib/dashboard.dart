import "package:flutter/material.dart";
import "package:graphql_flutter/graphql_flutter.dart";
import "package:renty/gqlclient/rentyQuery.dart";
import 'package:renty/models/user/user.dart';

class DashboardPage extends StatefulWidget {
  final String token;
  DashboardPage({this.token});
  @override
  State<StatefulWidget> createState() => _DashboardPage();
}

class _DashboardPage extends State<DashboardPage> {
  RentyQuery queries = RentyQuery();
  User user;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Query(
          options: QueryOptions(
            documentNode: gql(
                RentyQuery.user()), // this is the query string you just created
            pollInterval: 1000,
          ),
          builder: (QueryResult result,
              {VoidCallback refetch, FetchMore fetchMore}) {
            if (result.hasException) {
              return Text(result.exception.toString());
            }
            if (result.loading) {
              return CircularProgressIndicator();
            }
            user = User.fromHashMap(result.data['user']);
            return Card(
              child: Row(children: <Widget>[
                CircleAvatar(
                  minRadius: 50.3,
                  child: Text(user.name),
                ),
                Padding(
                  padding: EdgeInsets.all(30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("${user.civi} ${user.firstName} ${user.lastName}"),
                      Text(
                          "${user.email.value} ${user.email.verified ? '✔️' : '❌'}"),
                      Text(user.createdAt.toString()),
                      Text(user.updatedAt.toString()),
                      Padding(
                        padding: EdgeInsets.only(top: 20, left: 20),
                        child: RaisedButton.icon(
                            label: Text("refresh"),
                            icon: Icon(Icons.refresh),
                            onPressed: refetch),
                      )
                    ],
                  ),
                ),
              ]),
            );
          },
        )
      ],
    );
  }
}
