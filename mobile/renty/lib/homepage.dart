import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:renty/dashboard.dart';
import 'package:renty/gqlclient/config.dart';
import 'package:renty/login.dart';
import 'package:renty/map.dart';
import 'package:renty/mydocuments.dart';
import 'package:renty/register.dart';
import 'package:renty/utils/config.dart';
import 'package:renty/utils/consts.dart';
import 'package:renty/utils/googlesignin.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
    print(data);
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
    print(notification);
  }
  return null;
}

class HomePage extends StatefulWidget {
  final SharedPreferences preferences;
  final String fcmToken;

  HomePage({Key key, this.preferences, this.fcmToken}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState(prefs: preferences);
}

class _HomePageState extends State<HomePage> {
  final GraphQLClient client = GraphQLConfiguration().clientToQuery();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final SharedPreferences prefs;
  String fcmToken;
  String _authToken;
  var _page = 0;

  getlogin(String token) => setState(() {
        _authToken = token;
      });

  _HomePageState({this.prefs, this.fcmToken});
  @override
  void initState() {
    super.initState();
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print("onMessage: $message");
          // _showItemDialog(message);
        },
        onLaunch: (Map<String, dynamic> message) async {
          print("onLaunch: $message");
          // _navigateToItemDetail(message);
        },
        onResume: (Map<String, dynamic> message) async {
          print("onResume: $message");
          // _navigateToItemDetail(message);
        },
        onBackgroundMessage: myBackgroundMessageHandler);
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((v) => setState(() {
          fcmToken = v;
        }));
    if (this.prefs != null) {
      var istoken = this.prefs.get(Config.authTokenPrefKey);
      setState(() {
        _authToken = istoken;
      });
    }
  }

  void _doLogout() async {
    var isgogol = await GoogleSignInObject.isSignedIn();
    if (isgogol) {
      try {
        await GoogleSignInObject.disconnect();
      } catch (e) {
        print(e);
      }
    }
    await this.prefs.clear();
    await this.client.cache.reset();
    setState(() {
      this._page = 0;
      this._authToken = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    var appPages = _authToken == null
        ? <Widget>[
            LoginPage(
              authToken: _authToken,
              notificationID: fcmToken,
              callback: getlogin,
            ),
            RegisterPage(
              notificationID: fcmToken,
            ),
          ]
        : <Widget>[
            DashboardPage(
              token: _authToken,
            ),
            MyDocumentsPage(
              token: _authToken,
            ),
            MapPage()
          ];

    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          _authToken != null
              ? IconButton(
                  icon: Icon(Icons.exit_to_app),
                  onPressed: _doLogout,
                )
              : Container()
        ],
        title: Text(
          AppTitle,
          textScaleFactor: 1.6,
        ),
      ),
      bottomNavigationBar: _authToken != null
          ? BottomNavigationBar(
              currentIndex: _page,
              onTap: (index) => setState(() => _page = index),
              items: [
                  BottomNavigationBarItem(
                      title: Text("Settings"),
                      icon: Icon(
                        Icons.settings,
                        color: Colors.lightBlue,
                      )),
                  BottomNavigationBarItem(
                      title: Text("My documents"),
                      icon: Icon(Icons.file_upload, color: Colors.lightBlue)),
                  BottomNavigationBarItem(
                      title: Text("Explore"),
                      icon: Icon(Icons.map, color: Colors.lightBlue)),
                ])
          : null,
      body: Center(child: appPages[_page]),
      floatingActionButton: _authToken == null
          ? FloatingActionButton(
              child: Icon(_page == 0 ? Icons.person_add : Icons.person_outline),
              onPressed: () {
                setState(() {
                  _page == 0 ? _page += 1 : _page--;
                });
              },
            )
          : null,
    );
  }
}
