import 'package:renty/models/asset.dart';
import 'package:renty/models/label.dart';

/// DocumentType is the real estate rental needed doc
enum DocumentType { IDDOC, PROOFOFINCOME, EMPLOYMENTPROOF, PROOFOFADDRESS }
DocumentType DTfromString(String str) =>
    DocumentType.values.firstWhere((element) => element.Value == str);

extension on DocumentType {
  String get Value => this.toString().split('.').last;
}

///
///```
/// type UserDocument {
///   label: Label!
///   asset: Asset!
///   doctype: DocumentType!
///   createdAt: String!
///   updatedAt: String!
/// }
/// ```
class Document {
  final Label label;
  final Asset asset;
  final DocumentType documentType;
  final DateTime createdAt;
  final DateTime updatedAt;
  Document(
      {this.label,
      this.asset,
      this.documentType,
      this.createdAt,
      this.updatedAt});
  String get doctype => documentType.Value;
  factory Document.fromHashMap(Map<String, dynamic> map) {
    return Document(
        label: Label.fromHashMap(map["label"]),
        asset: Asset.fromHashMap(map["asset"]),
        documentType:
            map["doctype"] != null ? DTfromString(map["doctype"]) : null,
        createdAt:
            map["createdAt"] != null ? DateTime.parse(map["createdAt"]) : null,
        updatedAt:
            map["updatedAt"] != null ? DateTime.parse(map["updatedAt"]) : null);
  }
}
