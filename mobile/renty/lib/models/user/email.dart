class Email {
  final String value;
  final bool verified;
  Email({this.value, this.verified});
  factory Email.fromJson(Map<String, dynamic> json) {
    return Email(
        value: json["value"] ?? "no Email",
        verified: json["verified"] ?? false);
  }
}
