import 'package:renty/models/label.dart';
import 'package:renty/models/offers/rentoffers.dart';
import 'package:renty/models/user/email.dart';
import 'package:renty/models/user/phone.dart';

enum Civility { MS, MR, MX, MSS }

extension on Civility {
  String get Value => this.toString().split('.').last;
}

Civility CivifromString(String str) =>
    Civility.values.firstWhere((element) => element.Value == str);

extension on LangageCode {
  String get Value => this.toString().split('.').last;
}

/// `
/// type User {
///   name: String!
///   firstName: String
///   lastName: String
///   civility: Civility!
///   preferedLangage: LangageCode!
///   likedOffers: [RentOffer!]
///   email: Email
///   phone: PhoneNumber
///   devices: [Device!]
///   createdAt: String!
///   updatedAt: String!
///   }
/// `

class User {
  final String name;
  final String firstName;
  final String lastName;
  final Civility civility;
  final LangageCode preferedLangage;
  final RentOffers likedOffers;
  final Email email;
  final Phone phone;
  final DateTime createdAt;
  final DateTime updatedAt;
  String get civi => civility.Value;
  String get lang => preferedLangage.Value;
  User(
      {this.name,
      this.firstName,
      this.lastName,
      this.civility,
      this.preferedLangage,
      this.likedOffers,
      this.email,
      this.phone,
      this.createdAt,
      this.updatedAt});

  factory User.fromHashMap(Map<String, dynamic> map) {
    return User(
        name: map["name"] ?? "",
        firstName: map["firstName"] ?? "",
        lastName: map["lastName"] ?? "",
        civility: map["civility"] != null
            ? CivifromString(map["civility"])
            : Civility.MX,
        preferedLangage: map["preferedLangage"] != null
            ? LCfromString(map["preferedLangage"])
            : LangageCode.EN,
        // likedOffers: RentOffers.fromJson(json["likedOffers"]) ?? [],
        email: map["email"] != null ? Email.fromJson(map["email"]) : null,
        phone: map["phone"] != null ? Phone.fromJson(map["phone"]) : null,
        createdAt:
            map["createdAt"] != null ? DateTime.parse(map["createdAt"]) : null,
        updatedAt:
            map["updatedAt"] != null ? DateTime.parse(map["updatedAt"]) : null);
  }
}
