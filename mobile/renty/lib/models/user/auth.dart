/// ```
/// type Auth {
///    token: String!
///    type: String!
///    otp: Boolean!
///    createdAt: String!
///    validity: String!
///  }
/// ```
class Auth {
  final String token;
  final String type;
  final bool otp;
  final DateTime createdAt;
  final String validity;
  Auth({this.token, this.type, this.otp, this.createdAt, this.validity});
  factory Auth.fromJson(Map<String, dynamic> json) {
    return Auth(
        token: json["token"] ?? "",
        type: json["type"] ?? "Auth",
        otp: json["otp"] ?? false,
        createdAt: DateTime.parse(json["createdAt"]) ?? DateTime.now(),
        validity: json["validity"] ?? "");
  }
}
