///```
/// type PhoneNumber {
///   value: String!
///   countryCode: Int!
///   verified: Boolean!
///  }
/// ```
class Phone {
  final String value;
  final int countryCode;
  final bool verified;
  Phone({this.value, this.countryCode, this.verified});
  factory Phone.fromJson(Map<String, dynamic> json) {
    return Phone(
        value: json["value"] ?? "No phone",
        countryCode: int.tryParse(json["countryCode"]) ?? 0,
        verified: json["verified"] ?? false);
  }
}
