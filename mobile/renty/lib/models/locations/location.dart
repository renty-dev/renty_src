import 'package:latlong/latlong.dart';

/// ```
/// type Location {
///   latitude: Float!
///   longitude: Float!
///   metadatas: String
/// }
/// ```
/// Float GraphQL Type are converted to `double` in Dart
class Location {
  final double latitude;
  final double longitude;
  final LatLng latlng;
  final String metadatas;

  LatLng get Latlng => LatLng(longitude, latitude);
  Location({this.latitude, this.longitude, this.latlng, this.metadatas});
  factory Location.fromHashMap(Map<String, dynamic> map) {
    return Location(
        latitude: map["latitude"] != null
            ? double.parse(map['latitude'].toString())
            : null,
        longitude: map["longitude"] != null
            ? double.parse(map['longitude'].toString())
            : null,
        metadatas: map["metadatas"] ?? "");
  }
}
