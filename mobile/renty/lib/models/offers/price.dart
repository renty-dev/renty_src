enum Currency { EUR, USD, BTC }

extension on Currency {
  String get Value => this.toString().split('.').last;
}

Currency CurfromString(String str) =>
    Currency.values.firstWhere((element) => element.Value == str);

/// ```
/// type Price {
///  curr: Currency!
///  value: Float!
///  createdAt: String!
///  updatedAt: String!
/// }
/// ```
class Price {
  final double value;
  final Currency currency;
  final DateTime createdAt;
  final DateTime updatedAt;
  Price({this.value, this.currency, this.createdAt, this.updatedAt});
  String get curr => currency.Value;
  factory Price.fromHashMap(Map<String, dynamic> map) {
    return Price(
        value:
            map["value"] != null ? double.parse(map["value"].toString()) : null,
        currency: map["curr"] != null ? CurfromString(map["curr"]) : null,
        createdAt:
            map["createdAt"] != null ? DateTime.parse(map["createdAt"]) : null,
        updatedAt:
            map["updatedAt"] != null ? DateTime.parse(map["updatedAt"]) : null);
  }
}
