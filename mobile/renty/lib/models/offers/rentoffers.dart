import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:renty/models/offers/rentoffer.dart';

class RentOffers {
  final List<RentOffer> rentoffers;
  RentOffers({this.rentoffers});
  factory RentOffers.fromJson(List<dynamic> json) {
    List<RentOffer> qts = List<RentOffer>();
    if (json != null && json.isNotEmpty) {
      qts = json.map((q) => RentOffer.fromHashMap(q)).toList();
    }
    return RentOffers(rentoffers: qts);
  }
  RentOffer operator [](int i) => rentoffers[i];
  int len() => rentoffers.length;
  List<Marker> markers(BuildContext ctx) {
    return rentoffers
        .map<Marker>((e) => Marker(
            point: e.location.Latlng,
            builder: (ctx) => IconButton(
                  icon: Icon(Icons.home),
                  color: Colors.lightBlue,
                  onPressed: () => Scaffold.of(ctx).showSnackBar(SnackBar(
                      backgroundColor: Colors.blueAccent,
                      action: SnackBarAction(
                          label: "🔎",
                          onPressed: () =>
                              showDialog(context: ctx, child: e.details)),
                      duration: Duration(
                        seconds: 8,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      content: Container(
                        width: MediaQuery.of(ctx).size.width,
                        child: e.overview,
                      ))),
                )))
        .toList();
  }
}
