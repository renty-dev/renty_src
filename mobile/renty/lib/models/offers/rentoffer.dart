import 'package:flutter/material.dart';
import 'package:renty/models/asset.dart';
import 'package:renty/models/label.dart';
import 'package:renty/models/locations/location.dart';
import 'package:renty/models/offers/price.dart';

///
/// ```
/// type RentOffer {
///  id: String
///  area: Area!
///  price: [Price!]!
///  title: [Label!]!
///  assets: [Asset!]!
///  location: Location!
///  description: [Label!]!
///  availableFrom: String!
///  createdAt: String!
///  updatedAt: String!
/// }
/// ```
class RentOffer {
  final String id;
  final Price price;
  final Label title;
  final Asset assets;
  final Location location;
  final String availableFrom;
  final DateTime createdAt;
  final DateTime updatedAt;

  RentOffer(
      {this.id,
      this.price,
      this.title,
      this.assets,
      this.location,
      this.availableFrom,
      this.createdAt,
      this.updatedAt});

  Widget get overview => Text(
        "${price.value.toString()} ${price.curr} - ${title.text}",
        overflow: TextOverflow.ellipsis,
        softWrap: false,
      );
  Widget get details => Dialog(
        backgroundColor: Colors.lightBlue,
        insetPadding: EdgeInsets.all(30),
        child: Card(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[Image.network(assets.url)],
        )),
      );
  factory RentOffer.fromHashMap(Map<String, dynamic> map) {
    return RentOffer(
        id: map['id'],
        price: map["price"] != null ? Price.fromHashMap(map["price"][0]) : null,
        title: map["title"] != null ? Label.fromHashMap(map["title"][0]) : null,
        assets:
            map["assets"] != null ? Asset.fromHashMap(map["assets"][0]) : null,
        location: map["location"] != null
            ? Location.fromHashMap(map["location"])
            : null,
        availableFrom: map['availableFrom'],
        createdAt:
            map["createdAt"] != null ? DateTime.parse(map["createdAt"]) : null,
        updatedAt:
            map["updatedAt"] != null ? DateTime.parse(map["updatedAt"]) : null);
  }
}
