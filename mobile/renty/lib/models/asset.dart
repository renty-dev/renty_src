enum ContentType { PHOTO, VIDEO, DOCUMENT }

extension on ContentType {
  String get Value => this.toString().split('.').last;
}

ContentType CTfromString(String str) =>
    ContentType.values.firstWhere((element) => element.Value == str);

/// ```
/// type Asset {
///   url: String!
///   name: String!
///   type: ContentType!
///   createdAt: String!
///   updatedAt: String!
/// }
/// ```
class Asset {
  final String url;
  final String name;
  final ContentType contentType;
  final DateTime createdAt;
  final DateTime updatedAt;
  Asset(
      {this.url, this.name, this.contentType, this.createdAt, this.updatedAt});
  String get type => contentType.Value;
  factory Asset.fromHashMap(Map<String, dynamic> map) {
    return Asset(
        url: map["url"] ?? "",
        name: map["name"] ?? "",
        contentType: map["type"] != null ? CTfromString(map["type"]) : null,
        createdAt:
            map["createdAt"] != null ? DateTime.parse(map["createdAt"]) : null,
        updatedAt:
            map["updatedAt"] != null ? DateTime.parse(map["updatedAt"]) : null);
  }
}
