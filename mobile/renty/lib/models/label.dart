enum LangageCode {
  EN,
  FR,
  ES,
  IT,
  PL,
}

extension on LangageCode {
  String get Value => this.toString().split('.').last;
}

LangageCode LCfromString(String str) =>
    LangageCode.values.firstWhere((element) => element.Value == str);

/// ```
/// type Label {
///  text: String!
///  lang: LangageCode!
///  createdAt: String!
///  updatedAt: String!
/// }
/// ```
class Label {
  final String text;
  final LangageCode langageCode;
  final DateTime createdAt;
  final DateTime updatedAt;
  Label({this.text, this.langageCode, this.createdAt, this.updatedAt});
  String get lang => langageCode.Value;
  factory Label.fromHashMap(Map<String, dynamic> map) {
    return Label(
        text: map["text"] ?? "",
        langageCode:
            map["lang"] != null ? LCfromString(map["lang"]) : LangageCode.EN,
        createdAt:
            map["createdAt"] != null ? DateTime.parse(map["createdAt"]) : null,
        updatedAt:
            map["updatedAt"] != null ? DateTime.parse(map["updatedAt"]) : null);
  }
}
