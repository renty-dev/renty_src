import "package:flutter/material.dart";
import 'package:flutter_map/plugin_api.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:latlong/latlong.dart';
import 'package:renty/gqlclient/config.dart';
import 'package:renty/gqlclient/rentyQuery.dart';
import 'package:renty/models/offers/rentoffers.dart';

class MapPage extends StatefulWidget {
  MapPage();
  @override
  State<StatefulWidget> createState() => _MapPage();
}

class _MapPage extends State<MapPage> {
  RentOffers rentoffers;
  List<Marker> offerloc;
  GraphQLConfiguration graphQLConfiguration = GraphQLConfiguration();
  var token =
      "pk.eyJ1IjoiZGF2ZS1sb3BldXIiLCJhIjoiY2s2dzVkMTl2MDhlOTNlcXZ1N2o5eHNkNiJ9.6nm9cmaZzJU85LTjo0oR6Q";

  void _showSnackbar(
    String text,
    MaterialColor color,
  ) =>
      Scaffold.of(context).showSnackBar(SnackBar(
        action: SnackBarAction(
            label: "❌",
            onPressed: () => Scaffold.of(context).hideCurrentSnackBar()),
        content: Text(
          text,
          textScaleFactor: 1.1,
        ),
        elevation: 10,
        backgroundColor: color,
        behavior: SnackBarBehavior.fixed,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      ));
  void getoffer() async {
    GraphQLClient _client = graphQLConfiguration.clientToQuery();

    QueryResult result = await _client.query(
      QueryOptions(
          documentNode: gql(RentyQuery.rentoffers()), pollInterval: 4000),
    );
    if (!result.hasException) {
      List<dynamic> repository = result.data["offers"];
      if (repository.isNotEmpty) {
        setState(() {
          rentoffers = RentOffers.fromJson(repository);
        });
        _showSnackbar("Got  ${rentoffers.len()} offers !", Colors.lightBlue);
      } else {
        _showSnackbar("No real estate rent offers yet", Colors.lightBlue);
      }
    } else {
      _showSnackbar(result.exception.toString(), Colors.red);
    }
  }

  @override
  void initState() {
    getoffer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Flexible(
          child: FlutterMap(
        options: MapOptions(
          center: LatLng(44.8333, -0.5667),
          zoom: 13.0,
        ),
        layers: [
          TileLayerOptions(
            urlTemplate:
                "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=" +
                    token,
            additionalOptions: {'id': "mapbox/streets-v11"},
            subdomains: ['a', 'b', 'c'],
          ),
          rentoffers != null
              ? MarkerLayerOptions(markers: rentoffers.markers(context))
              : MarkerLayerOptions()
        ],
      )),
    ]);
  }
}
