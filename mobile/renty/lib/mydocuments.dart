import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'models/documents/document.dart';

class MyDocumentsPage extends StatefulWidget {
  final String token;
  MyDocumentsPage({this.token});
  @override
  State<StatefulWidget> createState() => _MyDocumentsPage();
}

class _MyDocumentsPage extends State<MyDocumentsPage> {
  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(
        documentNode: gql('''
              query {
                   mydocuments {
                     label {
                       text
                     }
                     asset {
                       name
                       url
                       type
                       createdAt
                       updatedAt
                     }
                     doctype
                     createdAt
                     updatedAt
                   }
               }'''),
      ),
      // Just like in apollo refetch() could be used to manually trigger a refetch
      // while fetchMore() can be used for pagination purpose
      builder: (QueryResult result,
          {VoidCallback refetch, FetchMore fetchMore}) {
        if (result.hasException) {
          return Text(result.exception.toString());
        }
        if (result.loading) {
          return CircularProgressIndicator();
        }
        List<dynamic> repository = result.data['mydocuments'];
        if (repository.isEmpty) {
          return Card(
            child: Row(
              children: <Widget>[
                Text("No document yet see below to upload one !")
              ],
            ),
          );
        }
        return RefreshIndicator(
            child: ListView.builder(
                itemCount: repository.length,
                itemBuilder: (ctx, index) {
                  Document onedoc = Document.fromHashMap(repository[index]);
                  return Card(
                    elevation: 6,
                    margin: EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(onedoc.doctype),
                              Text(onedoc.label.text)
                            ]),
                        Row(children: <Widget>[
                          Text(onedoc.asset.type),
                        ]),
                        Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(onedoc.createdAt.toString()),
                              Text(onedoc.updatedAt.toString())
                            ])
                      ],
                    ),
                  );
                }),
            onRefresh: refetch);
      },
    );
  }
}
