import 'dart:async';

import 'package:renty/homepage.dart';
import 'package:sentry/sentry.dart';
import 'package:flutter/material.dart';
import "package:graphql_flutter/graphql_flutter.dart";
import 'package:renty/utils/config.dart';
import 'package:renty/utils/consts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'gqlclient/config.dart';

GraphQLConfiguration graphQLConfiguration = GraphQLConfiguration();
final SentryClient sentry = SentryClient(dsn: Config.sentryDSN);



void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  var prefs = await SharedPreferences.getInstance();
  FlutterError.onError = (details, {bool forceReport = false}) {
    try {
      sentry.captureException(
        exception: details.exception,
        stackTrace: details.stack,
      );
    } catch (e) {
      print('Sending report to sentry.io failed: $e');
    } finally {
      // Also use Flutter's pretty error logging to the device's console.
      FlutterError.dumpErrorToConsole(details, forceReport: forceReport);
    }
  };
  runZoned(
    () => runApp(
      GraphQLProvider(
        client: graphQLConfiguration.client,
        child: CacheProvider(
            child: MyApp(
          prefs: prefs,
        )),
      ),
    ),
    onError: (Object error, StackTrace stackTrace) {
      try {
        sentry.captureException(
          exception: error,
          stackTrace: stackTrace,
        );
        print('Error sent to sentry.io: $error');
      } catch (e) {
        print('Sending report to sentry.io failed: $e');
        print('Original error: $error');
      }
    },
  );
}

class MyApp extends StatelessWidget {
  final SharedPreferences prefs;
  final String fcmToken;
  MyApp({this.prefs, this.fcmToken});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: AppTitle,
      // debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(
        preferences: prefs,
      ),
    );
  }
}

