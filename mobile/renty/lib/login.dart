import "package:flutter/material.dart";
import "package:graphql_flutter/graphql_flutter.dart";
import 'package:renty/gqlclient/rentyQuery.dart';
import 'package:renty/utils/config.dart';
import 'package:renty/utils/consts.dart';
import 'package:renty/utils/googlesignin.dart';
import 'package:renty/utils/jwt.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'gqlclient/config.dart';
import 'gqlclient/rentyQuery.dart';

class LoginPage extends StatefulWidget {
  final String notificationID;
  final String authToken;
  final Function(String) callback;
  LoginPage({this.authToken, this.callback, this.notificationID});
  @override
  State<StatefulWidget> createState() => _LoginPage();
}

class _LoginPage extends State<LoginPage> {
  static GraphQLConfiguration graphQLConfiguration = GraphQLConfiguration();
  GraphQLClient _client = graphQLConfiguration.clientToQuery();

  static var _nameTextController = TextEditingController();
  static var _passwordTextController = TextEditingController();
  var _isLoading = false;

  @override
  void dispose() {
    if (!this.mounted) {
      _nameTextController.dispose();
      _passwordTextController.dispose();
    }
    super.dispose();
  }

  void _showSnackbar(
    String text,
    MaterialColor color,
  ) =>
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          text,
          textScaleFactor: 1.1,
          overflow: TextOverflow.ellipsis,
        ),
        elevation: 10,
        backgroundColor: color,
        behavior: SnackBarBehavior.fixed,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      ));

  Future<void> _handleGoogleSignIn() async {
    try {
      await GoogleSignInObject.signIn();
      var token = await GoogleSignInObject.currentUser.authentication;
      attempSocialLogin(token.idToken);
    } catch (error) {
      print(error);
      _showSnackbar("Oh oh seems there is an error : $error", Colors.red);
    }
  }

  void attempSocialLogin(String idtoken) async {
    final prefs = await SharedPreferences.getInstance();
    print(widget.notificationID);
    setState(() => _isLoading = true);
    QueryResult result = await _client.query(QueryOptions(
      documentNode: gql(
        RentyQuery.sociallogin(idtoken, widget.notificationID),
      ),
    ));
    if (!result.hasException) {
      var tokenPayload;
      print(result.data);
      var token = result.data["socialLogin"] != null
          ? result.data["socialLogin"]["token"]
          : null;
      if (token != null) {
        tokenPayload = ParseJwt(token);
        var name = tokenPayload["sub"] as String;
        name = name.split(':')[0];
        await prefs.setString(Config.authTokenPrefKey, token);
        widget.callback(token);
        _showSnackbar(WelcomeMessage(name), Colors.lightBlue);
      }
    } else {
      _showSnackbar(result.exception.toString(), Colors.red);
    }
    setState(() => _isLoading = false);
  }

  void attemptLogin(String name, String password) async {
    final prefs = await SharedPreferences.getInstance();
    setState(() => _isLoading = true);
    QueryResult result = await _client.query(QueryOptions(
      documentNode: gql(
        RentyQuery.login(name, password, widget.notificationID),
      ),
    ));
    if (!result.hasException) {
      var token = result.data["login"]["token"];
      if (token != null) {
        await prefs.setString(Config.authTokenPrefKey, token);
        widget.callback(token);
        _showSnackbar(WelcomeMessage(name), Colors.lightBlue);
      }
    } else {
      _showSnackbar(result.exception.toString(), Colors.red);
    }
    setState(() {
      _isLoading = false;
    });
  }

  final _nameField = TextField(
    controller: _nameTextController,
    obscureText: false,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: NameInputHint,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );
  final _passwordField = TextField(
    controller: _passwordTextController,
    obscureText: true,
    keyboardType: TextInputType.visiblePassword,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: PasswordInputHint,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(padding: EdgeInsets.all(10), child: _nameField),
        Padding(
          padding: EdgeInsets.all(10),
          child: _passwordField,
        ),
        Padding(
            padding: EdgeInsets.only(bottom: 20),
            child: !_isLoading
                ? RaisedButton(
                    color: Colors.lightBlue,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(40)),
                    onPressed: () => attemptLogin(
                        _nameTextController.text, _passwordTextController.text),
                    child: Icon(
                      Icons.lock_open,
                      color: Colors.white,
                    ),
                  )
                : CircularProgressIndicator()),
        SignInButton(_handleGoogleSignIn),
      ],
    );
  }
}
