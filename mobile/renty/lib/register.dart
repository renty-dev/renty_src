import "package:flutter/material.dart";
import "package:graphql_flutter/graphql_flutter.dart";
import 'package:renty/gqlclient/rentyMutation.dart';
import 'package:renty/utils/consts.dart';
import 'package:renty/utils/googlesignin.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'gqlclient/config.dart';

class RegisterPage extends StatefulWidget {
  final String notificationID;
  RegisterPage({this.notificationID});
  @override
  State<StatefulWidget> createState() => _RegisterPage();
}

class _RegisterPage extends State<RegisterPage> {
  static GraphQLConfiguration graphQLConfiguration = GraphQLConfiguration();
  GraphQLClient _client = graphQLConfiguration.clientToQuery();
  static var _nameTextController = TextEditingController();
  static var _emailTextController = TextEditingController();
  static var _passwordTextController = TextEditingController();

  @override
  void dispose() {
    if (!this.mounted) {
      _nameTextController.dispose();
      _emailTextController.dispose();
      _passwordTextController.dispose();
    }
    super.dispose();
  }

  void _showSnackbar(
    String text,
    MaterialColor color,
  ) =>
      Scaffold.of(context).showSnackBar(SnackBar(
        action: SnackBarAction(
            label: "X",
            onPressed: () => Scaffold.of(context).hideCurrentSnackBar()),
        content: Text(
          text,
          textScaleFactor: 1.1,
        ),
        elevation: 10,
        backgroundColor: color,
        behavior: SnackBarBehavior.fixed,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      ));
  void attemptRegister(String name, String email, String password) async {
    final prefs = await SharedPreferences.getInstance();

    QueryResult result = await _client.mutate(
      MutationOptions(
        documentNode: gql(RentyMutation.register(
            name, email, password, this.widget.notificationID)),
      ),
    );
    if (!result.hasException) {
      var username = result.data["register"]["name"];
      if (username != null) {
        await prefs.setString("username", username);
        _showSnackbar(RegistrationSucess(username), Colors.lightBlue);
      }
    } else {
      _showSnackbar(BadRegistration, Colors.red);
      print(result.exception);
    }
  }

  void attemptSocialRegister(String idtoken) async {
    final prefs = await SharedPreferences.getInstance();

    QueryResult result = await _client.mutate(
      MutationOptions(
        documentNode: gql(
            RentyMutation.socialregister(idtoken, this.widget.notificationID)),
      ),
    );
    if (!result.hasException) {
      var username = result.data["socialregister"]["name"];
      if (username != null) {
        await prefs.setString("username", username);
        _showSnackbar(RegistrationSucess(username), Colors.lightBlue);
      }
    } else {
      _showSnackbar(BadRegistration, Colors.red);
      print(result.exception);
    }
  }

  Future<void> _handleRegister() async {
    try {
      await GoogleSignInObject.signIn();
      _showSnackbar(
        "Welcome ${GoogleSignInObject.currentUser.displayName}",
        Colors.blue,
      );
      var token = await GoogleSignInObject.currentUser.authentication;
      await attemptSocialRegister(token.idToken);
    } catch (error) {
      print(error);
    }
  }

  final _nameField = TextField(
    controller: _nameTextController,
    obscureText: false,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: NameRegisterHint,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );
  final _emailField = TextField(
    controller: _emailTextController,
    obscureText: false,
    keyboardType: TextInputType.emailAddress,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: EmailRgisterHint,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );
  final _passwordField = TextField(
    controller: _passwordTextController,
    obscureText: true,
    keyboardType: TextInputType.visiblePassword,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: PasswordRegisterHint,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(top: 5),
        children: <Widget>[
          Padding(padding: EdgeInsets.all(5), child: _nameField),
          Padding(padding: EdgeInsets.all(5), child: _emailField),
          Padding(
            padding: EdgeInsets.all(5),
            child: _passwordField,
          ),
          Padding(
            padding: EdgeInsets.all(5),
            child: Text(
              MandatoryText,
              textScaleFactor: 0.8,
              style: TextStyle(color: Colors.blueGrey),
            ),
          ),
          Padding(
              padding: EdgeInsets.all(5),
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40)),
                color: Colors.lightBlue,
                onPressed: () => attemptRegister(_nameTextController.text,
                    _emailTextController.text, _passwordTextController.text),
                child: Icon(
                  Icons.check,
                  color: Colors.white,
                ),
              )),
          GoogleRegisterButton(_handleRegister)
        ],
      ),
    );
  }
}
