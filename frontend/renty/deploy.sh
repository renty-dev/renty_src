#!/bin/bash
#    _____             _
#   |  __ \           | |
#   | |  | | ___ _ __ | | ___  _   _
#   | |  | |/ _ \ '_ \| |/ _ \| | | |
#   | |__| |  __/ |_) | | (_) | |_| |
#   |_____/ \___| .__/|_|\___/ \__, |
#               | |             __/ |
#               |_|            |___/
###
######## Renty - 2020
if [ -v $ENV ]; then ENV=staging; fi
source ".$ENV.env"
ZEIT_API="https://api.zeit.co/v1"
ZEIT_DEPLOY_PATH="/integrations/deploy/"
ZEIT_DEPLOY_HOOK=""$ZEIT_API""$ZEIT_DEPLOY_PATH""$ZEIT_DEPLOY_KEY""
## Cleaning previous builds if exist
rm -rf __sapper__/export 2&>/dev/null
rm -rf ../../../../renty_webapp/*
## Building static version of our webapp
yarn "export"
## Updating graphql endpoint
sed -i 's@http:\/\/localhost:4000\/query@'"${GQL_ENDPOINT}"'@' __sapper__/export/client/*.js
## Copying generated build file to renty_webapp dedicated repository
cp -r __sapper__/export/* ../../../renty_webapp
## Adding route mapping conf
cp conf/now.json ../../../renty_webapp
## Commit all changes
cd ../../../renty_webapp
git add .
git commit -m "chore(renty_webapp): Add $(date) version"
git push origin master
curl -s "$ZEIT_DEPLOY_HOOK" | jq
cd - &>/dev/null
echo "🚀 Done !"
