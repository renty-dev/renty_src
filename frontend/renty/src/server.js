import sirv from "sirv";
import polka from "polka";
import { createServer } from "https";
import { readFileSync } from "fs";
import compression from "compression";
import * as sapper from "@sapper/server";

const { PORT, NODE_ENV, FACEBOOK_DEV } = process.env;
const dev = NODE_ENV === "development";
const options = dev
  ? {
      key: readFileSync("ssl/dev.key"),
      cert: readFileSync("ssl/dev.crt"),
    }
  : {};
const { handler } = polka().use(
  compression({ threshold: 0 }),
  sirv("static", { dev }),
  sapper.middleware()
);

dev && FACEBOOK_DEV
  ? createServer(options, handler).listen(PORT, (err) => {
      if (err) {
        console.error("error", err);
      } else {
        console.info("> Listening on https://localhost:" + PORT);
      }
    })
  : polka()
      .use(
        compression({ threshold: 0 }),
        sirv("static", { dev }),
        sapper.middleware()
      )
      .listen(PORT, (err) => {
        if (err) console.log("error", err);
      });
