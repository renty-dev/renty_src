package main

import (
	"fmt"
	"os/exec"
	"strings"
	"time"
)

func main() {
	begining := time.Now()
	res, err := exec.Command("python3", "main.py").Output()
	if err != nil {
		fmt.Printf("Error : %s\n", err.Error())
	}
	elapsed := time.Since(begining)
	// fmt.Printf("Got res : \n[%s]", string(res), elapsed.Minutes)
	loc := strings.Split(string(res), "LOC")
	location := strings.Trim(loc[0], " ")
	if len(location) > 0 {
		fmt.Printf("Got LOC : %s (in %0.2f s)\n", location, elapsed.Seconds())
	}
}
