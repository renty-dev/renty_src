import spacy

nlp = spacy.load('fr_core_news_md')

# Contenu de l'annonce.
ex = """
    Loue un studio de 30 m2 rénové, à 150m de la place de la Victoire, dans un environnement très calme, avec accès sécurisé.
    Entrée avec placard, salle de bains avec baignoire et wc, grand séjour avec parquet, coin cuisine aménagée et emplacement pour lave-linge.
    Loyer mensuel 550 euros, charges 35 euros (eau, ordures ménagères, charges locatives, hors électricité).
    Engagements de longue durée privilégiés.
    Agences s'abstenir, merci.
"""

# tokenization
doc = nlp(ex)

# check
for entity in doc.ents:
    print(entity.text, entity.label_)
