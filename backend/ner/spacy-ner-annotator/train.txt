Idéalement situé à deux pas du tramway (Place de la Victoire), dans une petite rue donnant sur la rue Ste Catherine, ce studio est proche de tout : centre historique, transports, fac, restaurants, bars, boulangerie, carrefour market...
Entièrement refait à neuf avec des prestations haut de gamme, le studio est composé :
- D'un coin cuisine équipé (Lave linge, Frigo, micro-ondes, plaques vitro céramique, vaisselle, poêles, couverts, assiettes, verres...), avec un retour bar ainsi que deux tabourets.
- D'un coin salon avec écran plat mural, une chaine hi-fi, ainsi qu'un canapé et sa table basse.
- D'un coin chambre avec un lit 2pers. (séparable de l'espace cuisine / salon via un rideau coulissant). Draps, oreillers et couverture neufs sont également inclus.
Depuis le salon, une porte coulissante en verre trempé donne accès à une grande salle de bains avec WC, équipée d'une douche à l'italienne, d'un meuble vasque (deux grands tiroirs), avec son miroir éclairé ainsi qu'un dressing avec étagères et penderies.
Equipements supplémentaires : table, fer à repasser, balai, sceau et serpillières.
Loyer: 665€
+ Charges: 25€ (l'eau est comprise)
Caution : 2 mois de loyers
Garanties exigées : acte de caution solidaire !
Appartement libre de suite.
(Suite aux nombreuses demandes, 1er contact par email pour éviter les touristes : merci de préciser votre situation et vos revenus + garant)