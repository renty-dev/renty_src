package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

var (
	// ToVisit ...
	ToVisit = []string{
		"renty-dev-87c95aef34",
		"renty-gqlserver-dev",
		"renty-geoip-dev",
		"renty-scrapper-dev",
	}
)

func handler(ctx context.Context) (*events.APIGatewayProxyResponse, error) {
	var err error
	var resps = 0
	var bodyByte []byte
	var response *http.Response
	var nRequest *http.Request
	var reDelay = (10 * int(time.Second)) / len(ToVisit)
	var client = &http.Client{
		Timeout: time.Duration(reDelay) * time.Second,
	}

	for _, val := range ToVisit {
		reqURL := fmt.Sprintf("https://%s.herokuapp.com/", val)
		if nRequest, err = http.NewRequest("GET", reqURL, nil); err != nil {
			fmt.Printf("Error(geoip/GetGeoip): %s\n", err.Error())
		}
		nRequest.Header.Add("DontWorry", "ItsjustMe")
		fmt.Printf("Visiting [%s] at [%s]\n", val, reqURL)
		if response, err = client.Do(nRequest); err != nil {
			fmt.Printf("Error(geoip/GetGeoip): %s\n", err.Error())
		} else {
			fmt.Printf("Got resp [%v] |%s]\n", response.StatusCode, response.Status)
			defer response.Body.Close()
			resps++
		}

	}
	var JSONResp = map[string]string{
		"Message": fmt.Sprintf("Hit %v targets", resps),
	}
	if bodyByte, err = json.Marshal(JSONResp); err != nil {
		return nil, err
	}

	return &events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers:    map[string]string{"Content-type": "application/json"},
		Body:       string(bodyByte),
	}, nil
}

func main() {
	// Make the handler available for Remote Procedure Call by AWS Lambda

	lambda.Start(handler)
}
