package authy

import (
	"fmt"
	"os"
	"regexp"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

var (
	// AdminRole is the string representing the admin role value
	AdminRole = "admin"
	// SigningKey is the 'secret' JWT signing (private) key
	SigningKey = "SUPERSECRETTOBECHANGED"
	// ErrAccessDenied is the response message when a client request a non authorized access
	ErrAccessDenied = map[string]string{"message": "Ttttt ... You need a valid Authorization bro !"}
)

func init() {
	var signingKeyEnv = os.Getenv("SIGN_KEY")
	var adminRoleValueEnv = os.Getenv("ADMIN_ROLE")
	if signingKeyEnv != "" {
		SigningKey = signingKeyEnv
	}
	if adminRoleValueEnv != "" {
		AdminRole = adminRoleValueEnv
	}
}

func trimAuthHeader(authorization string) string {
	re := regexp.MustCompile(`(?i)bearer`)
	if re.Match([]byte(authorization)) {
		authorization = re.ReplaceAllString(authorization, "")
		authorization = strings.Trim(authorization, " ")
	}
	return authorization
}

func hasAdminRole(jwtsubject string) bool {
	userInfos := strings.Split(jwtsubject, ":")
	if len(userInfos) > 0 {
		var roleValue = strings.ToLower(userInfos[1])
		if roleValue == AdminRole {
			return true
		}
	}
	return false
}

// CheckAuth parse and check Authorization header field, allowing or rejecting client request
func CheckAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		var claims jwt.StandardClaims
		authHeader := c.GetHeader("Authorization")
		authHeader = trimAuthHeader(authHeader)
		if _, err = jwt.ParseWithClaims(authHeader, &claims, func(token *jwt.Token) (interface{}, error) {
			return []byte(SigningKey), nil
		}); err != nil {
			fmt.Printf("Error(authy/CheckAuth): %s\n", err.Error())
			c.AbortWithStatusJSON(401, ErrAccessDenied)
			return
		}
		if hasAdminRole(claims.Subject) {
			c.Next()
			return
		}
		c.AbortWithStatusJSON(401, ErrAccessDenied)
		return
	}
}
