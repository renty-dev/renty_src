module geoip

go 1.14

require (
	github.com/apex/gateway v1.1.1
	github.com/aws/aws-lambda-go v1.16.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.2
	github.com/oschwald/geoip2-golang v1.4.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/tj/assert v0.0.0-20190920132354-ee03d75cd160 // indirect
)
