package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"seed/generated/prisma"

	"golang.org/x/crypto/bcrypt"
)

var (
	defaultUserName         = "michel"
	defaultUserPassword     = "michel"
	defaultUserEmail        = "micheluser@renty.io"
	defaultUserPWDHash      = ""
	defaultAdminName        = "patrick"
	defaultAdminPassword    = "patrick"
	defaultAdminEmail       = "patrickadmin@renty.io"
	defaultAdminPWDHash     = ""
	defaultScrapperName     = "bernard"
	defaultScrapperPassword = "bernard"
	defaultScrapperEmail    = "bernardscrapper@renty.io"
	defaultScrapperPWDHash  = ""
)

var prismaClient = prisma.New(nil)

func init() {
	var err error
	var pwdHash []byte
	if os.Getenv("ENV") == "PROD" {
		defaultUserName = os.Getenv("SEED_USER_NAME")
		defaultUserPassword = os.Getenv("SEED_USER_PWD")
		defaultUserEmail = os.Getenv("SEED_USER_EMAIL")
		defaultAdminName = os.Getenv("SEED_ADMIN_NAME")
		defaultAdminPassword = os.Getenv("SEED_ADMIN_PWD")
		defaultAdminEmail = os.Getenv("SEED_ADMIN_EMAIL")
		defaultScrapperName = os.Getenv("SEED_SCRAPPER_NAME")
		defaultScrapperPassword = os.Getenv("SEED_SCRAPPER_PWD")
		defaultScrapperEmail = os.Getenv("SEED_SCRAPPER_EMAIL")

	}
	if pwdHash, err = bcrypt.GenerateFromPassword([]byte(defaultUserPassword), bcrypt.MinCost); err != nil {
		log.Fatal(err)
	}
	defaultUserPWDHash = string(pwdHash)
	if pwdHash, err = bcrypt.GenerateFromPassword([]byte(defaultAdminPassword), bcrypt.MinCost); err != nil {
		log.Fatal(err)
	}
	defaultAdminPWDHash = string(pwdHash)
	if pwdHash, err = bcrypt.GenerateFromPassword([]byte(defaultScrapperPassword), bcrypt.MinCost); err != nil {
		log.Fatal(err)
	}
	defaultScrapperPWDHash = string(pwdHash)
}

func createRegularUser() (*prisma.User, error) {
	role := prisma.RoleUser
	defMail := "defaultuser@renty.io"
	return prismaClient.CreateUser(
		prisma.UserCreateInput{
			Name:     "user_" + defaultUserName,
			Password: defaultUserPWDHash,
			Role:     &role,
			Email: &prisma.EmailCreateOneInput{
				Create: &prisma.EmailCreateInput{
					Value: defMail,
				},
			},
		},
	).Exec(context.TODO())
}

func createAdminUser() (*prisma.User, error) {
	role := prisma.RoleAdmin
	defMail := "defaultadmin@renty.io"
	return prismaClient.CreateUser(
		prisma.UserCreateInput{
			Name:     "admin_" + defaultAdminName,
			Password: defaultAdminPWDHash,
			Role:     &role,
			Email: &prisma.EmailCreateOneInput{
				Create: &prisma.EmailCreateInput{
					Value: defMail,
				},
			},
		},
	).Exec(context.TODO())
}

func createScrapperUser() (*prisma.User, error) {
	role := prisma.RoleAdmin
	defMail := "defaultscrapper@renty.io"
	return prismaClient.CreateUser(
		prisma.UserCreateInput{
			Name:     "scrapper_" + defaultScrapperName,
			Password: defaultScrapperPWDHash,
			Role:     &role,
			Email: &prisma.EmailCreateOneInput{
				Create: &prisma.EmailCreateInput{
					Value: defMail,
				},
			},
		},
	).Exec(context.TODO())
}

func main() {
	var err error
	var user *prisma.User

	if user, err = createRegularUser(); err != nil {
		// log.Fatal(err)
		fmt.Printf("Error(createRegularUser): %s\n", err.Error())
	} else {
		fmt.Printf("Info(createRegularUser): Created Regular User : %+v\n", user)
	}

	if user, err = createAdminUser(); err != nil {
		// log.Fatal(err)
		fmt.Printf("Error(createAdminUser): %s\n", err.Error())
	} else {
		fmt.Printf("Info(createAdminUser): Created Admin User : %+v\n", user)
	}

	if user, err = createScrapperUser(); err != nil {
		// log.Fatal(err)
		fmt.Printf("Error(createScrapperUser): %s\n", err.Error())
	} else {
		fmt.Printf("Info(createScrapperUser): Created Scrapper User : %+v\n", user)
	}

}
