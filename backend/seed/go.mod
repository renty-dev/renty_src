module seed

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/machinebox/graphql v0.2.2
	github.com/matryer/is v1.3.0 // indirect
	github.com/mitchellh/mapstructure v1.1.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prisma/prisma-client-lib-go v0.0.0-20181017161110-68a1f9908416
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d
)
