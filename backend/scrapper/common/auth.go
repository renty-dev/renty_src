package common

import (
	"encoding/json"
	"os"
	"regexp"
	"scrapper/models"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

// Auth check jwt token
func Auth(input string) *models.AskParams {
	var data models.AskParams
	if err := json.Unmarshal([]byte(input), &data); err != nil {
		return nil
	}
	var claims jwt.StandardClaims
	re := regexp.MustCompile(`(?i)bearer`)
	authHeader := data.Authorization
	if re.Match([]byte(authHeader)) {
		authHeader = re.ReplaceAllString(authHeader, "")
		authHeader = strings.Trim(authHeader, " ")
	}
	if _, err := jwt.ParseWithClaims(authHeader, &claims, func(token *jwt.Token) (interface{}, error) {
		jwtSecret := os.Getenv("SIGN8KEY")
		if jwtSecret == "" {
			jwtSecret = "SUPERSECRETTOBECHANGED"
		}
		return []byte(jwtSecret), nil
	}); err != nil || !strings.Contains(claims.Subject, "ADMIN") {
		return nil
	}
	return &data
}
