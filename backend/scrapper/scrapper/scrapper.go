package scrapper

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"scrapper/common"
	"scrapper/generated/prisma"
	"scrapper/models"
	"scrapper/runner"
	"strconv"
	"strings"

	"github.com/gorilla/websocket"
)

// ScrapperName is the admin for scrapping upload
var ScrapperName = os.Getenv("SCRAPPER_NAME")

// Scrapper is the scrapper config struct
type Scrapper struct {
	PrismaClient *prisma.Client
	Upgrader     websocket.Upgrader
	Name         string
}

func (s *Scrapper) leboncoin(r runner.Runner, pageNb int) (bool, error) {
	lbcRunner := runner.LebonCoinRunner{
		Runner: r,
	}
	lbcRunner.DoScrapping(pageNb)
	return true, nil
}

func (s *Scrapper) seloger(r runner.Runner, pageNb int) (bool, error) {
	selogerRunner := runner.SelogerRunner{
		Runner: r,
	}
	selogerRunner.DoScrapping(pageNb)
	return true, nil
}

func (s *Scrapper) pap(r runner.Runner, pageNb int) (bool, error) {
	papRunner := runner.PapRunner{
		Runner: r,
	}
	papRunner.DoScrapping(pageNb)
	return true, nil
}

func (s *Scrapper) pong(c *websocket.Conn) (bool, error) {
	msg := "🏓  Pong"
	if err := c.WriteJSON(&models.ExecResponse{
		Sucess: true,
		Author: &ScrapperName,
		Result: &msg,
	}); err != nil {
		return false, err
	}
	return false, nil
}

// RunCommands parse the ExecParams.command and execute it
func (s *Scrapper) RunCommands(message string, c *websocket.Conn) (bool, error) {
	tokens := strings.Split(message, " ")
	command := strings.Trim(tokens[0], "/")
	if len(tokens) >= 3 {
		target := tokens[1]
		page := tokens[2]
		if command == "run" {
			if pageNb, err := strconv.Atoi(page); err == nil && pageNb > 0 {
				runnerConf := runner.Runner{
					PrismaClient: s.PrismaClient,
					Conn:         c,
					Collector:    runner.Collector,
				}

				switch target {
				case "LBC":
					return s.leboncoin(runnerConf, pageNb)
				case "SELOGER":
					return s.seloger(runnerConf, pageNb)
				case "PAP":
					return s.pap(runnerConf, pageNb)
				}
			}
			return false, fmt.Errorf("Failed to run scrapping")
		}
	} else if len(tokens) == 1 && command == "ping" {
		return s.pong(c)
	}
	return false, fmt.Errorf("%s: command not found", message)
}

// ExecLive is the websocket controller to run scrapping
func (s *Scrapper) ExecLive(w http.ResponseWriter, r *http.Request) {
	var err error
	var message []byte
	// var params ExecParams
	var c *websocket.Conn

	if c, err = s.Upgrader.Upgrade(w, r, nil); err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()
	for {
		if _, message, err = c.ReadMessage(); err != nil {
			if !strings.Contains(err.Error(), "close 1005") {
				log.Println("Error : ", err)
			} else {
				fmt.Printf("Client disconnect :)\n")
			}
			break
		}

		if msg := common.Auth(string(message)); msg != nil {
			if _, err = s.RunCommands(msg.Command, c); err != nil {
				errmsg := err.Error()
				ress := models.ExecResponse{
					Sucess: false,
					Author: &ScrapperName,
					Error:  &errmsg,
				}
				if err = c.WriteJSON(&ress); err != nil {
					log.Println("write:", err)
					break
				}
			}
		} else {
			errmsg := "🤷 Missing \"authorization\" key and/or value !"
			ress := models.ExecResponse{
				Sucess: false,
				Author: &ScrapperName,
				Error:  &errmsg,
			}
			if err = c.WriteJSON(&ress); err != nil {
				log.Println("write:", err)
				break
			}
		}
	}
}
