package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"scrapper/generated/prisma"
	"scrapper/scrapper"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		fmt.Printf("Got request from origin %s\n", r.RemoteAddr)
		return true
	},
} // use default options

var port = os.Getenv("PORT")

func init() {
	if port == "" {
		port = "4200"
	}
	if scrapper.ScrapperName == "" {
		scrapper.ScrapperName = "scrapper_bernard"
	}
	if os.Getenv("PRISMA_SECRET") == "" {
		logmsg := fmt.Errorf("Missing PRISMA_SECRET envaar, will not be able to save scrapping results")
		log.Fatal(logmsg)
	}
}

func main() {
	r := mux.NewRouter()

	scrappy := scrapper.Scrapper{
		PrismaClient: prisma.New(nil),
		Upgrader:     upgrader,
	}
	// @deprecated
	// r.HandleFunc("/scrapper/exec", scrapper.Exec).Methods("POST") // HTTP Endpoint @deprecated
	r.HandleFunc("/scrapper/live", scrappy.ExecLive) // WebSocket (with live logs)
	fmt.Printf("Info: Scrapper API is available at :\n=> http://locahost:%v/scrapper/exec\n=> ws://locahost/scrapper/live:%v/\n", port, port)
	if err := http.ListenAndServe(":"+port, r); err != nil {
		log.Fatal(err)
	}
}
