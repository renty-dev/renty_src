package runner

import (
	"context"
	"fmt"
	"scrapper/generated/prisma"
	"scrapper/geocoder"
	"scrapper/models"
	"strconv"
	"strings"
	"time"

	"github.com/gocolly/colly"
)

// SelogerRunner ...
type SelogerRunner struct {
	Runner
}

var rentoffers = []models.SelogerRentOffer{}
var rentOffersImageLinks = map[string][]models.SeLogerImageLink{}

// SaveAll ...
func (s *SelogerRunner) SaveAll(rentoffers []models.SelogerRentOffer) (int, int) {
	sucess, errors := 0, 0
	isprivate := false
	var timestamp = time.Now().Format(time.RFC3339)
	for i := range rentoffers {
		prismaImgInput := []prisma.AssetCreateInput{}
		for j, img := range rentoffers[i].ImageLinks {
			prismaImgInput = append(prismaImgInput, prisma.AssetCreateInput{
				Type: prisma.ContentTypePhoto,
				Name: fmt.Sprintf("%s_%v", rentoffers[i].ID.URL, j),
				Url:  img.URL,
			})
		}
		if _, err := s.PrismaClient.CreateRentOffer(prisma.RentOfferCreateInput{
			AvailableFrom: timestamp,
			SourceUrl:     &rentoffers[i].ID.URL,
			Area: prisma.AreaCreateOneInput{
				Create: &prisma.AreaCreateInput{
					Value: rentoffers[i].Area.Area,
				},
			},
			IsPrivate: &isprivate,
			Assets: &prisma.AssetCreateManyInput{
				Create: prismaImgInput,
			},
			Price: &prisma.PriceCreateManyInput{
				Create: []prisma.PriceCreateInput{
					prisma.PriceCreateInput{
						Curr:  prisma.CurrencyEur,
						Value: rentoffers[i].Price.Price,
					},
				},
			},
			Location: prisma.LocationCreateOneInput{
				Create: &prisma.LocationCreateInput{
					Latitude:  rentoffers[i].Location.Coords[0],
					Longitude: rentoffers[i].Location.Coords[1],
				},
			},
			Title: &prisma.LabelCreateManyInput{
				Create: []prisma.LabelCreateInput{
					prisma.LabelCreateInput{
						Text: fmt.Sprintf("Appartement %v m² %s", rentoffers[i].Area.Area, rentoffers[i].Location.Location),
					},
				},
			},
			CreatedBy: prisma.UserCreateOneInput{
				Connect: &prisma.UserWhereUniqueInput{
					Name: &ScrapperName,
				},
			},
		}).Exec(context.Background()); err != nil {
			errors++
		} else {
			sucess++
		}
	}
	return sucess, errors
}

// Visit is the entry point for seloger scrapping
func (s *SelogerRunner) Visit(startURL string) {
	s.Collector.OnHTML("div", func(e *colly.HTMLElement) {
		classRef := e.Attr("class")
		if strings.Contains(classRef, "Card__ContentZone") {
			seLogerRentOffer := models.SelogerRentOffer{}
			e.ForEach("a", func(i int, e *colly.HTMLElement) {
				linkRef := e.Attr("href")
				if strings.Contains(linkRef, "ListToDetail") {
					seLogerRentOffer.ID.URL = linkRef
					return
				}
			})
			e.ForEach("span", func(index int, span *colly.HTMLElement) {
				if index < 2 {
					locAttr := strings.ReplaceAll(span.Text, "-", " ")
					seLogerRentOffer.Location.Location += " " + locAttr
				} else {
					return
				}
			})
			e.ForEach("li", func(index int, li *colly.HTMLElement) {
				if len(li.Text) > 0 {
					if strings.Contains(li.Text, "m²") {
						areaStr := strings.Trim(li.Text, " m²")
						areaStr = strings.Replace(areaStr, ",", ".", 1)
						areaVal, _ := strconv.ParseFloat(areaStr, 64)
						seLogerRentOffer.Area.Area = areaVal
						return
					}
				}
			})
			e.ForEach("div", func(index int, div *colly.HTMLElement) {
				divclassRef := div.Attr("class")
				if strings.Contains(divclassRef, "Price_") && index > 3 {
					if index == 4 {
						offerPriceStr := div.Text
						offerPriceParse := strings.Split(offerPriceStr, " ")
						offerPriceVal, _ := strconv.ParseFloat(offerPriceParse[0], 64)
						seLogerRentOffer.Price.Price = offerPriceVal
						return
					}
				}
			})
			if seLogerRentOffer.ID.URL != "" {
				rentoffers = append(rentoffers, seLogerRentOffer)
				e.Request.Visit(seLogerRentOffer.ID.URL)
			}
		} else if strings.Contains(classRef, "Slide__ShowcaseMediaSlide") {
			offerID := e.Request.URL
			imageLink := e.Attr("data-background")
			if imageLink != "" {
				rentOffersImageLinks[offerID.String()] = append(rentOffersImageLinks[offerID.String()], models.SeLogerImageLink{
					URL: imageLink,
				})
			}

		}
	})
	s.Collector.Visit(startURL)
}

// Buildquery return the query string
func (s *SelogerRunner) Buildquery(page int) string {
	seLogerBaseURL := "https://www.seloger.com/list.htm"
	seLogerSearchParams := "projects=1&types=1&natures=1&places=[{ci%3A330063}]&price=NaN%2F700&enterprise=0&qsVersion=1.0"
	seLogerPageParam := fmt.Sprintf("LISTING-LISTpg=%v", page)
	seLogerURL := seLogerBaseURL + "?" + seLogerSearchParams + "&" + seLogerPageParam
	return seLogerURL
}

// DoScrapping ...
func (s *SelogerRunner) DoScrapping(page int) {
	startURL := s.Buildquery(page)
	s.Visit(startURL)
	for i := range rentoffers {
		var err error
		var geo *geocoder.FeatureCollection
		rentoffers[i].ImageLinks = rentOffersImageLinks[rentoffers[i].ID.URL]
		if geo, err = geocoder.DoSearch(rentoffers[i].Location.Location); err != nil {
			fmt.Printf("Failed to geocode %s : %s\n", rentoffers[i].Location.Location, err.Error())
		} else {
			rentoffers[i].Location.Coords = geo.Features[0].Coordinates
		}
	}
	sucess, errs := s.SaveAll(rentoffers)
	message := fmt.Sprintf("Saving %v rent offers (%v errors)", sucess, errs)
	fmt.Println(message)
	if err := s.Conn.WriteJSON(&models.ExecResponse{
		Sucess: true,
		Author: &ScrapperName,
		Result: &message,
	}); err != nil {
		fmt.Printf("Error(DoScrapping) : %s", err.Error())
	}
}
