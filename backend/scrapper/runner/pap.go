package runner

import (
	"fmt"
	"strings"

	"github.com/gocolly/colly"
)

// PapRunner ...
type PapRunner struct {
	Runner
}

// PapRentOffer ...
type PapRentOffer struct {
	URL         string
	Title       string
	Price       string
	Area        string
	Description string
	// Location []float64
}

var paprentoffers = map[string]PapRentOffer{}

// Visit ...
func (p *PapRunner) Visit(startURL string) {
	nrentOffer := PapRentOffer{}
	p.Collector.OnHTML("a", func(e *colly.HTMLElement) {
		refLink := e.Request.AbsoluteURL(e.Attr("href"))
		if strings.Contains(refLink, "/annonces/") {
			nrentOffer.URL = refLink
		}
		e.ForEach("li", func(i int, li *colly.HTMLElement) {
			if strings.Contains(li.Text, "m2") {
				nrentOffer.Area = li.Text
			}
		})
		e.ForEach("span", func(i int, span *colly.HTMLElement) {
			if len(span.Text) > 0 {
				if strings.Contains(span.Text, "Location") {
					nrentOffer.Title = span.Text
				} else if strings.Contains(span.Text, "€") {
					nrentOffer.Price = span.Text
				}
			}
		})
		// e.Request.Visit(rentOfferURL)
		if len(nrentOffer.URL) > 0 {
			paprentoffers[nrentOffer.URL] = nrentOffer
		}
	})
	p.Collector.OnHTML("p", func(e *colly.HTMLElement) {
		classRef := e.Attr("class")
		if strings.Contains(classRef, "item-description") {
			trimText := strings.Split(e.Text, "\n")
			trimstr := strings.Trim(trimText[2], " ")
			fmt.Printf("Found p : [%s]\n", trimstr)
		}
	})
	p.Collector.Visit(startURL)
}

// Buildquery ...
func (p *PapRunner) Buildquery(page int) string {
	baseURL := "https://www.pap.fr/annonce"
	queryParam := fmt.Sprintf("/locations-appartement-bordeaux-33-g43588-jusqu-a-700-euros-%v", page)
	startURL := baseURL + queryParam
	return startURL
}

// DoScrapping ...
func (p *PapRunner) DoScrapping(pageNb int) {
	startURL := p.Buildquery(pageNb)
	p.Visit(startURL)
	fmt.Printf("Got resp : %+v\n", paprentoffers)
}
