package runner

import (
	"log"
	"os"
	"scrapper/generated/prisma"

	"github.com/gocolly/colly"
	"github.com/gocolly/colly/proxy"
	"github.com/gorilla/websocket"
)

// UAS ...
var UAS = []string{
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36",
	"Mozilla/5.0 (Windows NT 10.0; rv:68.0) Gecko/20100101 Firefox/68.0",
}

// ScrapperName is the scrapper name
var ScrapperName = os.Getenv("SCRAPPER_NAME")

// Runner is the main scrapper method
type Runner struct {
	Collector    *colly.Collector
	PrismaClient *prisma.Client
	Conn         *websocket.Conn
}

// IRunner is the main scrappers interface
type IRunner interface {
	Visit(startURL string)
	DoScrapping(pages int)
	Buildquery(page int) string
	SaveAll(rentoffers interface{}) (int, int)
}

// Collector is the gocolly scrapper
var Collector = colly.NewCollector(colly.UserAgent(UAS[0]), colly.AllowURLRevisit())

func init() {
	proxyURL := os.Getenv("PROXY_URL")
	if proxyURL != "" {
		rp, err := proxy.RoundRobinProxySwitcher(proxyURL)
		if err != nil {
			log.Fatal(err)
		}
		Collector.SetProxyFunc(rp)
	}
	if ScrapperName == "" {
		ScrapperName = "scrapper_bernard"
	}
	Collector.OnRequest(func(r *colly.Request) {
		// Found this trick in github issue to evade DataDome scrapping protection
		r.Headers.Set("Accept", "*/*")
		r.Headers.Set("Accept-Language", "en-US,en;q=0.5")
		r.Headers.Set("Accept-Encoding", "gzip, deflate, br")
	})
}
