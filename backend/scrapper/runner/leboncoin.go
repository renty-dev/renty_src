package runner

import (
	"context"
	"fmt"
	"log"
	"os"
	"regexp"
	"scrapper/generated/prisma"
	"scrapper/geocoder"
	"scrapper/models"
	"strconv"
	"strings"
	"time"

	"github.com/gocolly/colly"
)

// LebonCoinRunner ...
type LebonCoinRunner struct {
	Runner
	rentOffers           []models.LBCRentOffer
	rentOfferTitlesInfos []models.LBCTitles
	rentOfferLinksInfos  []models.LBCLinks
	rentOfferDatesInfos  []models.LBCDates
	rentOfferImagesInfos []models.LBCIMGLinks
	rentOfferPriceInfos  []models.LBCPrice
	rentOfferAreaInfos   []models.LBCArea
}

func truncate(t time.Time) time.Time {
	return t.Truncate(24 * time.Hour)
}

// Visit need to be refactored (extract parsing functions)
func (l *LebonCoinRunner) Visit(startURL string) {
	// Find all links
	l.Collector.OnHTML("a", func(e *colly.HTMLElement) {
		var offerPriceVal float64
		re := regexp.MustCompile("\\d+") // Get digits
		offerTitle := e.Attr("title")
		offerLink := models.Leboncoin + e.Attr("href")
		offerDate := e.DOM.Find("p[class=mAnae]").Text()
		if len(offerTitle) > 0 && len(offerDate) > 0 &&
			strings.Contains(offerLink, "/locations/") &&
			!strings.Contains(offerLink, "/offres/") {
			offerPriceDom := e.DOM.Find("span[itemprop=priceCurrency]").Text()
			offerPriceStr := ""
			offerPriceArr := re.FindAllString(offerPriceDom, -1)
			for _, foo := range offerPriceArr {
				offerPriceStr += foo
			}
			linkID := models.LBCLinks{ID: offerLink}
			parseDate := strings.Split(offerDate, ", ")
			offerPriceVal, _ = strconv.ParseFloat(offerPriceStr, 64)
			if len(parseDate) > 0 && parseDate[0] == "Aujourd'hui" {
				parseDuration := strings.Split(parseDate[1], ":")
				durationString := parseDuration[0] + "h" + parseDuration[1] + "m"
				durationVal, _ := time.ParseDuration(durationString)
				offerDate = truncate(time.Now()).Add(durationVal).Format(time.RFC3339)
			} else if len(parseDate) > 0 && parseDate[0] == "Hier" {
				fmt.Printf("Trying to get date from [%s]\n", parseDate[1])
				parseDuration := strings.Split(parseDate[1], ":")
				durationString := parseDuration[0] + "h" + parseDuration[1] + "m"
				durationVal, _ := time.ParseDuration(durationString)
				yesterDay := time.Now().AddDate(0, 0, -1)
				offerDate = truncate(yesterDay).Add(durationVal).Format(time.RFC3339)
			} else {
				fmt.Printf("Trying to get date from [%v]\n", parseDate)
				if dateString, err := time.Parse("27 fév, 22:42", offerDate); err == nil {
					offerDate = dateString.Format(time.RFC3339)
				} else {
					fmt.Printf("Error trying to parse date %s : %s\n", dateString, err.Error())
				}
			}
			logzmsg := fmt.Sprintf("Found offer : [%s] [%0.2f] posted at : [%s]\n", offerTitle, offerPriceVal, offerDate)
			if l.Conn != nil {
				l.Conn.WriteJSON(&models.ExecLog{
					Message: &logzmsg,
					Author:  &ScrapperName,
				})
			}
			fmt.Printf(logzmsg)
			l.rentOfferLinksInfos = append(l.rentOfferLinksInfos, linkID)
			l.rentOfferTitlesInfos = append(l.rentOfferTitlesInfos, models.LBCTitles{
				LBCLinks: linkID,
				Text:     offerTitle,
			})
			l.rentOfferDatesInfos = append(l.rentOfferDatesInfos, models.LBCDates{
				LBCLinks: linkID,
				Date:     offerDate,
			})
			l.rentOfferPriceInfos = append(l.rentOfferPriceInfos, models.LBCPrice{
				LBCLinks: linkID,
				Price:    offerPriceVal,
			})
			e.Request.Visit(offerLink) // Visit the rent offers ones
		}

	})
	// Find all surfaces
	l.Collector.OnHTML("div[data-qa-id]", func(e *colly.HTMLElement) {
		if strings.Contains(e.Text, "Surface") {
			offerAreaInfo := strings.Split(e.Text, "Surface")
			if len(offerAreaInfo) >= 1 && len(offerAreaInfo[1]) <= 8 &&
				strings.Contains(offerAreaInfo[1], "m²") {
				id := fmt.Sprintf("%s", e.Request.URL)
				re := regexp.MustCompile("[0-9]+")
				area := re.FindAllString(offerAreaInfo[1], -1)
				if len(area) > 0 {
					// fmt.Printf("Got parsing res : %v for : %v\n", area, offerAreaInfo)
					if value, err := strconv.ParseFloat(area[0], 64); err == nil {
						fmt.Printf("Got surface %v %s\n", offerAreaInfo[1], e.Request.URL)
						l.rentOfferAreaInfos = append(l.rentOfferAreaInfos, models.LBCArea{
							LBCLinks: models.LBCLinks{ID: id},
							Area:     value,
						})
					} else {
						fmt.Printf("Error getting float %s\n", err.Error())
					}
				}
			}
		}
	})
	// Find all images
	l.Collector.OnHTML("img", func(e *colly.HTMLElement) {
		offerImageLink := e.Attr("src")
		if len(offerImageLink) > 0 && !strings.Contains(offerImageLink, "bo-logo") {
			offerLink := fmt.Sprintf("%s", e.Request.URL)
			linkID := models.LBCLinks{ID: offerLink}
			// fmt.Printf("Found image %s for offer %s\n", offerImageLink, e.Request.URL)
			l.rentOfferImagesInfos = append(l.rentOfferImagesInfos, models.LBCIMGLinks{
				LBCLinks: linkID,
				URL:      offerImageLink,
			})
		}
	})

	l.Collector.OnError(func(res *colly.Response, err error) {
		logs := fmt.Sprintf("Got error getting [ %s ] : %s\n", res.Request.URL, err.Error())
		if l.Conn != nil {
			l.Conn.WriteJSON(&models.ExecLog{
				Message: &logs,
				Author:  &ScrapperName,
			})
		}
		fmt.Fprint(os.Stderr, logs)
	})
	l.Collector.Visit(startURL)
}

// SaveOne save one leboncoin to db
func (l *LebonCoinRunner) saveOne(rentoffer models.LBCRentOffer) error {
	var err error
	isPrivate := !true // All scrapped offer are public by default
	prismaExec := l.PrismaClient.CreateRentOffer(prisma.RentOfferCreateInput{
		IsPrivate:     &isPrivate,
		AvailableFrom: rentoffer.Date,
		SourceUrl:     &rentoffer.Link,
		Area: prisma.AreaCreateOneInput{
			Create: &prisma.AreaCreateInput{
				Value: rentoffer.Area,
			},
		},
		Title: &prisma.LabelCreateManyInput{
			Create: []prisma.LabelCreateInput{
				prisma.LabelCreateInput{
					Text: rentoffer.Title,
				},
			},
		},
		Price: &prisma.PriceCreateManyInput{
			Create: []prisma.PriceCreateInput{
				prisma.PriceCreateInput{
					Curr:  prisma.CurrencyEur,
					Value: rentoffer.Price,
				},
			},
		},
		Assets: &prisma.AssetCreateManyInput{
			Create: []prisma.AssetCreateInput{
				prisma.AssetCreateInput{
					Type: prisma.ContentTypePhoto,
					Name: rentoffer.Link,
					Url:  rentoffer.ImageLinks[0],
				},
				prisma.AssetCreateInput{
					Type: prisma.ContentTypePhoto,
					Name: rentoffer.Link,
					Url:  rentoffer.ImageLinks[1],
				},
			},
		},
		Location: prisma.LocationCreateOneInput{
			Create: &prisma.LocationCreateInput{
				Latitude:  rentoffer.Location[0],
				Longitude: rentoffer.Location[1],
				Metadatas: rentoffer.LocationMetadatas,
			},
		},
		CreatedBy: prisma.UserCreateOneInput{
			Connect: &prisma.UserWhereUniqueInput{
				Name: &ScrapperName,
			},
		},
	})
	if _, err = prismaExec.Exec(context.Background()); err != nil {
		logzmsg := fmt.Sprintf("Error prisma : %s\n", err.Error())
		if l.Conn != nil {
			l.Conn.WriteJSON(&models.ExecLog{
				Author:  &ScrapperName,
				Message: &logzmsg,
			})
		}
		fmt.Print(logzmsg)
	} else {
		logzmsg := fmt.Sprintf("Saving %+v\n", rentoffer)
		if l.Conn != nil {
			l.Conn.WriteJSON(&models.ExecLog{
				Author:  &ScrapperName,
				Message: &logzmsg,
			})
		}
		fmt.Print(logzmsg)
	}
	return err
}

// SaveAll iterate through scrapping result and update them one by one (to be refactored)
func (l *LebonCoinRunner) SaveAll(rentoffers []models.LBCRentOffer) (int, int) {
	var err error
	saved := 0
	errors := 0
	for _, offer := range rentoffers {
		if len(offer.ImageLinks) > 1 {
			if err = l.saveOne(offer); err == nil {
				saved++
			} else {
				errors++
			}
		}
	}
	return saved, errors
}

// DoScrapping ...
func (l *LebonCoinRunner) DoScrapping(pages int) {
	// Clearing previous results
	l.rentOfferTitlesInfos = nil
	l.rentOfferLinksInfos = nil
	l.rentOfferDatesInfos = nil
	l.rentOfferImagesInfos = nil
	l.rentOfferPriceInfos = nil
	l.rentOfferAreaInfos = nil
	l.rentOffers = nil
	target := l.Buildquery(pages)
	logzmsg := "Starting query to " + target
	if l.Conn != nil {
		l.Conn.WriteJSON(&models.ExecLog{
			Author:  &ScrapperName,
			Message: &logzmsg,
		})
	}
	l.Visit(target)
	l.rentOffers = l.getFullOffers(
		l.rentOfferTitlesInfos,
		l.rentOfferLinksInfos,
		l.rentOfferDatesInfos,
		l.rentOfferImagesInfos,
		l.rentOfferPriceInfos,
		l.rentOfferAreaInfos,
	)
	l.getlatlong(l.rentOffers)
	saved, errors := l.SaveAll(l.rentOffers)
	messg := fmt.Sprintf("Done uploading %v new offers (%v errors) from %s\n", saved, errors, models.Leboncoin)
	sucess := true
	if saved == 0 || errors > 0 {
		sucess = false
	}
	res := models.ExecResponse{
		Sucess:   sucess,
		Author:   &ScrapperName,
		Result:   &messg,
		Uploaded: saved,
	}
	fmt.Printf(messg)
	if err := l.Conn.WriteJSON(&res); err != nil {
		log.Println("write:", err)
	}

}

// getFullOffers need to be refactored (too complex)
func (l *LebonCoinRunner) getFullOffers(
	titles []models.LBCTitles,
	links []models.LBCLinks,
	dates []models.LBCDates,
	imgs []models.LBCIMGLinks,
	prices []models.LBCPrice,
	areas []models.LBCArea,
) []models.LBCRentOffer {
	var fullRentOffers []models.LBCRentOffer
	for _, link := range links {
		nfoo := models.LBCRentOffer{
			Link: link.ID,
		}
		for _, title := range titles {
			if title.ID == link.ID {
				nfoo.Title = title.Text
				break
			}
		}
		for _, date := range dates {
			if date.ID == link.ID {
				nfoo.Date = date.Date
				break
			}
		}
		for _, price := range prices {
			if price.ID == link.ID {
				nfoo.Price = price.Price
				break
			}
		}
		for _, area := range areas {
			if area.ID == link.ID {
				nfoo.Area = area.Area
				break
			}
		}
		for _, img := range imgs {
			if img.ID == link.ID {
				nfoo.ImageLinks = append(nfoo.ImageLinks, img.URL)
			}
		}
		fullRentOffers = append(fullRentOffers, nfoo)
	}
	return fullRentOffers
}

func (l *LebonCoinRunner) getlatlong(offers []models.LBCRentOffer) {
	for index, offer := range offers {
		var err error
		var resp *geocoder.FeatureCollection
		var query = strings.Trim(offer.Title, "T2 - / m2 T3")
		query = strings.ToLower(query)
		if !strings.Contains(query, "Bordeaux") &&
			!strings.Contains(query, "bordeaux") {
			query += " Bordeaux"
		}
		if resp, err = geocoder.DoSearch(query); err != nil {
			fmt.Printf("Error geocoding : %s\n", err.Error())
		} else if len(resp.Features) > 0 && resp.Features[0].Properties.City == "Bordeaux" {
			// fmt.Printf("Got resp %v for %s\n", resp.Features[0], query)
			l.rentOffers[index].Location = resp.Features[0].Coordinates
			metas, _ := resp.Features[0].ToString()
			l.rentOffers[index].LocationMetadatas = metas
		} else {
			offers[index].Location = []float64{-0.57598, 44.85097}
		}
		fmt.Printf("Adding coordinates : %+v\n", offers[index].Location)
	}
}

// Buildquery ...
func (l *LebonCoinRunner) Buildquery(pageNb int) string {
	urlParams := ""
	params := map[string]string{
		models.LBCRealEstateCategoryKey: models.RentsCategoryID,
		models.LBCLocationCodeKey:       "Bordeaux__44.85097037673542_-0.5759878158908474",
		models.LBCRealEstateTypeKey:     models.FlatRealEstateType,
	}
	for key, val := range params {
		if urlParams == "" {
			urlParams += key + "=" + val
		} else {
			urlParams += "&" + key + "=" + val
		}
	}
	query := fmt.Sprintf(
		"%s/%s?%s&%s=%v",
		models.Leboncoin,
		models.LBCSearch,
		urlParams,
		models.LBCPage,
		pageNb,
	)
	return query
}
