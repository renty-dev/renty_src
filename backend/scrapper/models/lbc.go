package models

const (
	// Leboncoin is a french rent offer listing website
	Leboncoin = "https://www.leboncoin.fr"
	// LBCSearch ...
	LBCSearch = "recherche"
	// LBCPage is the page result index
	LBCPage = "page"
	// LBCRealEstateCategoryKey indicate the kind off real estate contract (colocation, classic rents ...)
	LBCRealEstateCategoryKey = "category"
	// LBCLocationCodeKey is the french regions code label
	LBCLocationCodeKey = "locations"
	// LBCRealEstateTypeKey indicate whether the good is a flat, a house etc
	LBCRealEstateTypeKey = "real_estate_type"
	// LBCRealEstateSellerType is c to c vs b to c
	LBCRealEstateSellerType = "owner_type"
	// LBCRealEstateEquipmentType indicate wether the good is equiped (furnished) or not
	LBCRealEstateEquipmentType = "furnished"
)

// LBCRentOffer ...
type LBCRentOffer struct {
	Title             string    `json:"title"`
	Link              string    `json:"link"`
	Date              string    `json:"date"`
	ImageLinks        []string  `json:"image_link"`
	Area              float64   `json:"area"`
	Price             float64   `json:"price"`
	Location          []float64 `json:"location"`
	LocationMetadatas *string   `json:"location_metadatas"`
}

// LBCLinks ...
type LBCLinks struct {
	ID string
}

// LBCTitles ...
type LBCTitles struct {
	LBCLinks
	Text string
}

// LBCDates ...
type LBCDates struct {
	LBCLinks
	Date string
}

// LBCArea ...
type LBCArea struct {
	LBCLinks
	Area float64
}

// LBCPrice ...
type LBCPrice struct {
	LBCLinks
	Price float64
}

// LBCIMGLinks ...
type LBCIMGLinks struct {
	LBCLinks
	URL string
}

// AddImage ...
func (a *LBCRentOffer) AddImage(v *LBCRentOffer) *LBCRentOffer {
	a.ImageLinks = append(a.ImageLinks, v.ImageLinks...)
	return a
}
