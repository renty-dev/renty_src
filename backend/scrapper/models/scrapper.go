package models

// AskParams ...
type AskParams struct {
	Author        *string `json:"author,omitempty"`
	Command       string  `json:"command"`
	Authorization string  `json:"authorization"`
}

// ExecParams ...
type ExecParams struct {
	TargetID string `json:"target_id"`
	Pages    int    `json:"pages"`
}

// ExecResponse ...
type ExecResponse struct {
	Sucess   bool    `json:"success"`
	Result   *string `json:"result"`
	Error    *string `json:"error"`
	Uploaded int     `json:"uploaded"`
	Author   *string `json:"author"`
}

// ExecLog ...
type ExecLog struct {
	Message *string `json:"message"`
	Author  *string `json:"author"`
}
