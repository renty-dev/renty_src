package models

// SeLogerLinks ...
type SeLogerLinks struct {
	URL string
}

// SeLogerAreaInfos ...
type SeLogerAreaInfos struct {
	Area float64
}

// SeLogerPrice ...
type SeLogerPrice struct {
	Price float64
}

// SeLogerLocation ...
type SeLogerLocation struct {
	Location string
	Coords   []float64
}

// SeLogerImageLink ...
type SeLogerImageLink struct {
	URL string
}

// SelogerRentOffer ...
type SelogerRentOffer struct {
	ID         SeLogerLinks
	Area       SeLogerAreaInfos
	Price      SeLogerPrice
	Location   SeLogerLocation
	ImageLinks []SeLogerImageLink
}
