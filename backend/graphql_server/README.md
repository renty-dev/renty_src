# GraphqlServer

Run `make` or `make help` in your terminal to see available commands

## A. <u>Lib go mod and submodules</u>

Use the **lib** go module root to write 'utils like' features and 
the nested sub-directories for independant features (see [single responsibilty principle](https://en.wikipedia.org/wiki/Single-responsibility_principle)).

1. <u>Middlewares</u>

Everything around the http ```context.Context``` implementation and the http req checking / blocking / updates... etc.

2. <u>Resolver</u>

The final resolvers for query and mutation types (mostly using the [prismaClient](./generated/prisma/prisma.go))

3. <u>Directive</u>

The 'resolvers' for the graphql [directives](./schema/directives.graphql) 


## B. <u>Generated*</u>

1. Prisma client

See the backend [README](../README.md) for how to update it.

2. Gqlgen Graphql server

3. Gqlgen Graphql models (for things like inputtype)

4. Stub (generated resolvers prototypes)

\* *do not edit "à la main", use `make codegen` to update*

## C. <u>Schema</u>

- What this API is all about ! Mostly consuming [datamodel.prisma](../datamodel.prisma) fields.