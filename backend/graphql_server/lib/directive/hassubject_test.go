package directive

import (
	"context"
	"graphql_server/generated/models"
	"graphql_server/lib"
	"testing"
)

func TestHasSubject(t *testing.T) {
	testCtx := lib.ContextKey("subject")
	ctx := context.Background()
	badctx := context.WithValue(ctx, testCtx, "invalid")
	goodctx := context.WithValue(ctx, testCtx, "AUTH")
	if _, err := HasSubject(ctx, nil, dummyResolver, models.SubjectOtp); err == nil {
		t.Error("HasRole failed to block empty context")
	}
	if _, err := HasSubject(badctx, nil, dummyResolver, models.SubjectOtp); err == nil {
		t.Error("HasRole failed to block invalid subject")
	}
	if _, err := HasSubject(goodctx, nil, dummyResolver, models.SubjectAuth); err != nil {
		t.Error("HasRole failed to authorize valid subject")
	}
}
