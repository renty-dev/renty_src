package directive

import (
	"context"
	"graphql_server/generated/prisma"
	"graphql_server/lib"
	"testing"
)

func dummyResolver(ctx context.Context) (res interface{}, err error) { return nil, nil }

func TestHasRole(t *testing.T) {
	testCtx := lib.ContextKey("userrole")
	ctx := context.Background()
	badctx := context.WithValue(ctx, testCtx, "invalid")
	goodctx := context.WithValue(ctx, testCtx, "USER")
	if _, err := HasRole(ctx, nil, dummyResolver, prisma.RoleUser); err == nil {
		t.Error("HasRole failed to block empty context")
	}
	if _, err := HasRole(badctx, nil, dummyResolver, prisma.RoleUser); err == nil {
		t.Error("HasRole failed to block invalid role")
	}
	if _, err := HasRole(goodctx, nil, dummyResolver, prisma.RoleUser); err != nil {
		t.Error("HasRole failed to authorize valid role")
	}
}
