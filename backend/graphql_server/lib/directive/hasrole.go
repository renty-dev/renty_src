package directive

import (
	"context"
	"fmt"
	"graphql_server/generated/prisma"
	"graphql_server/lib"

	"github.com/99designs/gqlgen/graphql"
)

// HasRole check if the role in jwt key set in context match the schema role
func HasRole(ctx context.Context, obj interface{}, next graphql.Resolver, role prisma.Role) (interface{}, error) {
	var userroleCtx = lib.ContextKey("userrole")
	v := ctx.Value(userroleCtx)
	userRole := fmt.Sprintf("%v", v)
	if userRole != string(role) && (userRole != string(prisma.RoleAdmin)) {
		fmt.Printf("Want ROLE : %s , have : %s\n", role, userRole)
		return nil, fmt.Errorf("Access denied")
	}
	// or let it pass through
	return next(ctx)
}
