package directive

import (
	"context"
	"fmt"
	"graphql_server/generated/models"
	"graphql_server/lib"

	"github.com/99designs/gqlgen/graphql"
)

// HasSubject check if the role in jwt key set in context match the schema role
func HasSubject(ctx context.Context, obj interface{}, next graphql.Resolver, subject models.Subject) (interface{}, error) {
	var subjectCtx = lib.ContextKey("subject")
	v := ctx.Value(subjectCtx)
	if v != nil {
		str := v.(string)
		if str != string(subject) {
			return nil, fmt.Errorf("Access denied")
		}
	} else {
		return nil, fmt.Errorf("Access denied")
	}
	// or let it pass through
	return next(ctx)
}
