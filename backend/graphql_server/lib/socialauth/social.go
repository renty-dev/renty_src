package socialauth

import (
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"graphql_server/generated/prisma"
	"graphql_server/lib"
	"io/ioutil"
	"net/http"

	"github.com/dgrijalva/jwt-go"
)

const (
	googleSigningKeysURL = "https://www.googleapis.com/oauth2/v1/certs"
)

// GoogleClaims represent the fields in the jwt string provided by the id_token
type GoogleClaims struct {
	jwt.StandardClaims
	Name       string `json:"name"`
	Email      string `json:"email"`
	Picture    string `json:"picture"`
	GivenName  string `json:"given_name"`
	FamilyName string `json:"family_name"`
	Locale     string `json:"locale"`
	Verified   bool   `json:"email_verified"`
}

// GetTokenDatas deserialize the received jwt without checking its authenticity (usefull to get algo and public key id if its a rsa signed one)
func GetTokenDatas(IDToken string) (*jwt.Token, error) {
	var err error
	var tokenData *jwt.Token
	var nokeyfuncErr = "no Keyfunc was provided."
	if tokenData, err = jwt.Parse(IDToken, nil); err != nil && err.Error() != nokeyfuncErr {
		lib.LogError("mutation/SocialRegister", err.Error())
		return nil, fmt.Errorf("Bad token")
	}
	return tokenData, nil
}

// GetSigningKeys return the sso public keys to check the jwt token signature
func GetSigningKeys(sso prisma.AuthProvider, pubkeyID string) (*string, error) {
	var err error
	var res *http.Response
	var body []byte
	var resp map[string]string
	switch sso {
	case prisma.AuthProviderGoogle:
		{
			// Getting fresh public RSA256 PEM encoded public keys
			if res, err = http.Get(googleSigningKeysURL); err != nil {
				return nil, err
			}
			if body, err = ioutil.ReadAll(res.Body); err != nil {
				return nil, err
			}
			if json.Unmarshal(body, &resp); err != nil {
				return nil, err
			}
			googleKeyID := resp[pubkeyID]
			return &googleKeyID, nil
		}
	default:
		break
	}
	return nil, fmt.Errorf("No such auth provider")
}

// ParseGoogleClaims validate an idtoken objtained from a google signin or register in frontends clients
func ParseGoogleClaims(tokenData *jwt.Token) (*GoogleClaims, error) {
	var err error
	var publicKey *string
	var claims GoogleClaims
	var rsaPubKey *rsa.PublicKey
	pubkeyID := fmt.Sprintf("%v", tokenData.Header["kid"])
	if publicKey, err = GetSigningKeys(prisma.AuthProviderGoogle, pubkeyID); err != nil {
		lib.LogError("socialauth/ParseGoogleClaims 1", err.Error())
		return nil, fmt.Errorf("Bad token")
	}
	if rsaPubKey, err = jwt.ParseRSAPublicKeyFromPEM([]byte(*publicKey)); err != nil {
		lib.LogError("socialauth/ParseGoogleClaims 2", err.Error())
		return nil, fmt.Errorf("Bad token")
	}
	if _, err = jwt.ParseWithClaims(tokenData.Raw, &claims, func(token *jwt.Token) (interface{}, error) {
		return rsaPubKey, nil
	}); err != nil {
		lib.LogError("socialauth/ParseGoogleClaims 3", err.Error())
		return nil, fmt.Errorf("Bad token")
	}
	if err = claims.Valid(); err != nil {
		lib.LogError("socialauth/ParseGoogleClaims 4", err.Error())
		return nil, fmt.Errorf("Bad token")
	}
	return &claims, nil
}
