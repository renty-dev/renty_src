package dabucket

import (
	"context"
	"fmt"
	"graphql_server/lib"
	"log"
	"os"
	"strings"
	"testing"
)

var dummyReader = strings.NewReader("Just a dummy test text file")
var errNilContext = fmt.Errorf("Context is nil cannot keep going")
var testfile = BucketFile{
	Buffer: dummyReader,
	Fpath:  "test/test_delete.txt",
	Fname:  "test_delete.txt",
	Fsize:  dummyReader.Size(),
}

// This TestMain is not doing what he said but it's more a reminder for test
// teardown setup before running the dabucket tests
func TestMain(m *testing.M) {
	if lib.ServerConf.B2BUCKETAccount == "x" ||
		lib.ServerConf.B2BUCKETKey == "x" {
		log.Fatal("B2 Bucket secrets must be set before testing dabucket package")
	}
	if _, err := CopyFile(ctx, testfile); err != nil {
		log.Fatalf("Failed to run teardown : %s\n", err.Error())
	}
	os.Exit(m.Run())
}

var ctx = context.Background()

func TestCopyFile(t *testing.T) {
	var res *string
	var err error
	dummyReader := strings.NewReader("Just a dummy test text file")
	testfile := BucketFile{
		Buffer: dummyReader,
		Fpath:  "test/test.txt",
		Fname:  "test.txt",
		Fsize:  dummyReader.Size(),
	}

	if res, err = CopyFile(nil, testfile); err.Error() != errNilContext.Error() {
		t.Errorf("CopyFile should have return a nilcontext error want [%s] have [%s]\n", errNilContext.Error(), err.Error())
	}
	if res, err = CopyFile(ctx, testfile); err != nil {
		t.Errorf("Failed to upload test file : %s\n", err.Error())
	}
	if res == nil {
		t.Errorf("Failed to get file URL got nil\n")
	}
}

func TestDeleteFile(t *testing.T) {
	badfpath := "isabad fpath"
	testError := fmt.Errorf("Object [%s] is not in bucket", badfpath)
	if err := DeleteFile(nil, badfpath); err.Error() != errNilContext.Error() {
		t.Errorf("DeleteFile should have return a nilcontext error want [%s] have [%s]\n", errNilContext.Error(), err.Error())
	}
	if err := DeleteFile(ctx, badfpath); err.Error() != testError.Error() {
		t.Errorf("DeleteFile should have returned error [%s] got [%s]\n", testError.Error(), err.Error())
	}
	if err := DeleteFile(ctx, testfile.Fpath); err != nil {
		t.Errorf("Failed to delete test file : %s\n", err.Error())
	}
}
