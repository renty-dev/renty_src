package dabucket

import (
	"context"
	"fmt"
	"graphql_server/lib"
	"io"
	"os"

	"github.com/kurin/blazer/b2"
)

var daBucket *b2.Bucket

// BucketFile is a wrapper to save file from Graphql Upload scalar
type BucketFile struct {
	Buffer io.Reader
	Fpath  string
	Fname  string
	Fsize  int64
}

func init() {
	var err error
	var client *b2.Client
	var ctx = context.Background()
	if client, err = b2.NewClient(
		ctx,
		lib.ServerConf.B2BUCKETAccount,
		lib.ServerConf.B2BUCKETKey,
	); err != nil {
		lib.LogError("dabucket/init", err.Error())
		if lib.ServerConf.Stage != "dev" {
			os.Exit(1)
		}
	}
	if daBucket, err = client.Bucket(ctx, lib.ServerConf.B2BUCKETName); err != nil {
		lib.LogError("dabucket/init", err.Error())
		if lib.ServerConf.Stage != "dev" {
			os.Exit(1)
		}
	}
}

// DeleteFile delete a file in bucket
func DeleteFile(ctx context.Context, fpath string) error {
	var err error
	var obj *b2.Object

	if ctx == nil {
		return fmt.Errorf("Context is nil cannot keep going")
	}
	obj = daBucket.Object(fpath)
	if err = obj.Delete(ctx); err != nil {
		lib.LogError("dabucket/DeletFile", err.Error())
		return fmt.Errorf("Object [%s] is not in bucket", fpath)
	}
	return err
}

// CopyFile upload a file to B2 cloud storage bucket
func CopyFile(ctx context.Context, file BucketFile) (*string, error) {
	var obj *b2.Object

	if ctx == nil {
		return nil, fmt.Errorf("Context is nil cannot keep going")
	}
	if obj = daBucket.Object(file.Fpath); obj == nil {
		return nil, fmt.Errorf("Failed to load bucket Object")
	}
	w := obj.NewWriter(ctx)
	if _, err := io.Copy(w, file.Buffer); err != nil {
		if closeerr := w.Close(); closeerr != nil {
			lib.LogError("lib/dabucket", err.Error())
		}
		return nil, err
	}
	if err := w.Close(); err != nil {
		return nil, err
	}
	fileURL := obj.URL()
	return &fileURL, nil
}
