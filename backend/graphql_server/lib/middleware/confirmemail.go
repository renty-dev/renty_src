package middleware

import (
	"context"
	"encoding/json"
	"graphql_server/generated/prisma"
	"graphql_server/lib"

	"github.com/gin-gonic/gin"
)

// ConfirmEmail check if authorization code exist,
// confirm the related email in db
// and redirect to the frontend adress if success.
func ConfirmEmail() gin.HandlerFunc {
	// Code for the middleware...
	return func(c *gin.Context) {
		verified := true
		prismaClient := prisma.New(nil)
		confirmationCodeStr := c.Param("token")

		if len(confirmationCodeStr) == 36 {
			confirmationCode := prisma.Uuid(confirmationCodeStr)
			if _, err := prismaClient.UpdateEmail(prisma.EmailUpdateParams{
				Where: prisma.EmailWhereUniqueInput{
					ConfirmationCode: &confirmationCode,
				},
				Data: prisma.EmailUpdateInput{
					Verified:         &verified,
					ConfirmationCode: nil,
				},
			}).Exec(context.Background()); err != nil {
				lib.LogError("middleware/ConfirmEmail", err.Error())
				res, _ := json.Marshal(map[string]string{"Error": "Invalid request"})
				c.Writer.Header().Add("Content-Type", "application/json")
				if _, err := c.Writer.Write(res); err != nil {
					lib.LogError("middleware/ConfirmEmail", err.Error())
				}
				return
			}
		}
		c.Redirect(301, lib.ServerConf.MailConfirmRedirectURL)
	}
}
