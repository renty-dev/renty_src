package middleware

import (
	"graphql_server/generated/prisma"
	"graphql_server/generated/server"
	"graphql_server/lib/directive"
	"graphql_server/lib/resolvers"
	"net/http"
	"time"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

// GraphqlHandler is the main graphql API handler
func GraphqlHandler() gin.HandlerFunc {
	c := server.Config{
		Resolvers: &resolvers.Resolver{
			Prisma: prisma.New(nil),
		},
		Directives: server.DirectiveRoot{
			HasRole:    directive.HasRole,
			HasSubject: directive.HasSubject,
		}}
	srv := handler.NewDefaultServer(server.NewExecutableSchema(c))
	srv.AddTransport(transport.POST{})
	srv.AddTransport(&transport.Websocket{
		KeepAlivePingInterval: 10 * time.Second,
		Upgrader: websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool {
				// TODO: 🚨 Please define authorized hosts !
				return true
			},
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
		},
	})
	return func(c *gin.Context) {
		srv.ServeHTTP(c.Writer, c.Request)
	}
}
