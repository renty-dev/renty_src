package lib

import (
	"reflect"
	"testing"
)

func TestServerConf(t *testing.T) {
	v := reflect.ValueOf(ServerConf)
	typeOfS := v.Type()
	// Checking fields for empty strings
	for i := 0; i < v.NumField(); i++ {
		if v.Field(i).Interface() == "" {
			t.Errorf("Error for Field: %s unexpected Value: %v\n", typeOfS.Field(i).Name, v.Field(i).Interface())
		}
	}
	// Checking durations
	if ServerConf.TotpDuration.Minutes() <= 1 {
		t.Errorf("Error for field TotpDuration, duration should be > 1 min, got : %v\n", ServerConf.TotpDuration.Minutes())
	}
	if ServerConf.SessionDuration.Seconds() <= 0 {
		t.Errorf("Error for field SessionDuration, duration should be > 1 min, got : %v\n", ServerConf.SessionDuration.Minutes())
	}
}
