package resolvers

import (
	"context"
	"graphql_server/generated/prisma"
	"graphql_server/generated/server"
)

type userDocumentResolver struct{ *Resolver }

// UserDocument returns server.UserDocumentResolver implementation.
func (r *Resolver) UserDocument() server.UserDocumentResolver { return &userDocumentResolver{r} }

// Label return a user document associated labels
func (r *userDocumentResolver) Label(ctx context.Context, obj *prisma.UserDocument) (*prisma.Label, error) {
	return r.Prisma.UserDocument(prisma.UserDocumentWhereUniqueInput{
		ID: &obj.ID,
	}).Label().Exec(ctx)
}

// Asset return a user document associated assets
func (r *userDocumentResolver) Asset(ctx context.Context, obj *prisma.UserDocument) (*prisma.Asset, error) {
	return r.Prisma.UserDocument(prisma.UserDocumentWhereUniqueInput{
		ID: &obj.ID,
	}).Asset().Exec(ctx)
}
