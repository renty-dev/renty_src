package resolvers

import (
	"context"
	"encoding/json"
	"fmt"
	"graphql_server/generated/models"
	"graphql_server/generated/prisma"
	"graphql_server/lib"
	"graphql_server/lib/geoip"
	"graphql_server/lib/sendmail"
	"net"
	"strings"
	"time"

	"github.com/dgryski/trifles/uuid"
	ua "github.com/mileusna/useragent"
	"github.com/nyaruka/phonenumbers"
)

// MutationResolver is the mutations resolver root
type MutationResolver struct{ *Resolver }

//////////////////////////////////////////
///
/// 👥 User and registrations helpers
///

// RegisterDevice is an helper function to get a new device object
func RegisterDevice(userip string, useragent string, notifID *string) (*prisma.DeviceCreateInput, error) {
	var err error
	var geoinfoBytes []byte
	ndevice := prisma.DeviceCreateInput{
		UserAgent:      useragent,
		NotificationId: notifID,
		Ips: &prisma.DeviceCreateipsInput{
			Set: []string{userip},
		},
		Visits: &prisma.DeviceCreatevisitsInput{
			Set: []string{time.Now().Format(time.RFC3339)},
		},
	}
	geoIP := geoip.GetGeoip(userip)
	useragentInfos := ua.Parse(useragent)
	if useragentInfos.Mobile || // Is smartphone (web browser or app)
		strings.Contains(useragent, "Dart/") {
		mobile := prisma.DeviceTypeMobile
		ndevice.DeviceType = &mobile
	} else if useragentInfos.Desktop { // Is desktop (web browser)
		desktop := prisma.DeviceTypeDesktop
		ndevice.DeviceType = &desktop
	} else if useragentInfos.Tablet { // Is tablet (web browser or app)
		tablet := prisma.DeviceTypeTablet
		ndevice.DeviceType = &tablet
	} else {
		other := prisma.DeviceTypeOther
		ndevice.DeviceType = &other
	}
	if geoIP != nil {
		if geoinfoBytes, err = json.Marshal(&geoIP); err != nil {
			return nil, err
		}
		geoInfoString := string(geoinfoBytes)
		ndevice.GeoIp = &prisma.LocationCreateManyInput{
			Create: []prisma.LocationCreateInput{
				{
					Latitude:  geoIP.Lat,
					Longitude: geoIP.Lon,
					Metadatas: &geoInfoString,
				},
			},
		}
	}
	return &ndevice, nil
}

// RegisterEmail check and parse an email input to return a prisma input
func RegisterEmail(username string, emailinput *string) *prisma.EmailCreateInput {
	var err error
	var nEmail prisma.EmailCreateInput
	prismaClient := prisma.New(nil)

	if emailinput != nil {
		// Check if we dont allready have this email in db
		if exist, err := prismaClient.Email(prisma.EmailWhereUniqueInput{Value: emailinput}).Exists(context.Background()); err != nil || exist {
			return nil
		}
		// Check mail (lib)
		if err := sendmail.ValidateFormat(*emailinput); err != nil {
			return nil
		}
		hostInfo := strings.Split(*emailinput, "@")
		// Check
		if _, err = net.LookupMX(hostInfo[1]); err != nil {
			lib.LogError("mutation/RegisterEmail", err.Error())
			return nil
		}
		// Send confirmation email with a unique confirmation link
		confirmToken := uuid.UUIDv4()
		confirmLink := conf.URL + "/checkemail/" + confirmToken
		// TODO: @feat The below email template must come from a database
		template := sendmail.GetEmailValidationTemplate(username, confirmLink, lib.ServerConf.SupportEmail)
		if template == nil {
			randomstr := "Oh oh it's a bug ... "
			template = &randomstr
		}
		go sendmail.SendEmail(sendmail.Email{
			To:      []string{username + " <" + *emailinput + ">"},
			From:    lib.ServerConf.SMTPFromField,
			Subject: "Be ready to get a Home, Welcome to Renty !",
			Message: *template,
		})
		confirmationCode := prisma.Uuid(confirmToken)
		nEmail = prisma.EmailCreateInput{
			Value:            *emailinput,
			ConfirmationCode: &confirmationCode,
		}
		return &nEmail

	}
	return nil
}

// RegisterPhone check and parse an email input structure to return a prisma input
func RegisterPhone(phoneinput *models.PhoneInput) *prisma.PhoneNumberCreateInput {
	var err error
	var num *phonenumbers.PhoneNumber
	var nPhone prisma.PhoneNumberCreateInput

	if phoneinput != nil {
		// Check phone (regex)
		lib.LogInfo("resolvers/registerPhone", fmt.Sprintf("Got phone number %s", *phoneinput))
		// parse our phone number
		if num, err = phonenumbers.Parse(phoneinput.Value, string(phoneinput.CountryCode)); err != nil || num == nil {
			lib.LogError("mutation/RegisterPhone", err.Error())
			return nil
		}
		// format it using national format
		formattedNum := phonenumbers.Format(num, phonenumbers.INTERNATIONAL)
		// Send confirmation sms (twillio?)
		if !phonenumbers.IsValidNumber(num) {
			return nil
		}
		// Return the create one input
		nPhone = prisma.PhoneNumberCreateInput{
			Value:       formattedNum,
			CountryCode: num.GetCountryCode(),
		}
		return &nPhone
	}
	return nil
}
