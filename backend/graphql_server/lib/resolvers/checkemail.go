package resolvers

import (
	"context"
	"graphql_server/generated/prisma"
)

func (r *queryResolver) CheckEmail(ctx context.Context, email string) (bool, error) {
	return r.Prisma.Email(prisma.EmailWhereUniqueInput{
		Value: &email,
	}).Exists(ctx)
}
