package resolvers

import (
	"context"
	"graphql_server/generated/prisma"
	"graphql_server/generated/server"
)

// RentOffer is the resolver root for composite item RentOffer
func (r *Resolver) RentOffer() server.RentOfferResolver { return &rentOfferResolver{r} }

type rentOfferResolver struct{ *Resolver }

func (r *rentOfferResolver) Title(ctx context.Context, obj *prisma.RentOffer) ([]prisma.Label, error) {
	return r.Prisma.RentOffer(prisma.RentOfferWhereUniqueInput{ID: &obj.ID}).Title(nil).Exec(ctx)
}

func (r *rentOfferResolver) Description(ctx context.Context, obj *prisma.RentOffer) ([]prisma.Label, error) {
	return r.Prisma.RentOffer(prisma.RentOfferWhereUniqueInput{ID: &obj.ID}).Description(nil).Exec(ctx)
}

func (r *rentOfferResolver) Location(ctx context.Context, obj *prisma.RentOffer) (*prisma.Location, error) {
	return r.Prisma.RentOffer(prisma.RentOfferWhereUniqueInput{ID: &obj.ID}).Location().Exec(ctx)
}

func (r *rentOfferResolver) Assets(ctx context.Context, obj *prisma.RentOffer) ([]prisma.Asset, error) {
	return r.Prisma.RentOffer(prisma.RentOfferWhereUniqueInput{ID: &obj.ID}).Assets(nil).Exec(ctx)
}

func (r *rentOfferResolver) Price(ctx context.Context, obj *prisma.RentOffer) ([]prisma.Price, error) {
	return r.Prisma.RentOffer(prisma.RentOfferWhereUniqueInput{ID: &obj.ID}).Price(nil).Exec(ctx)
}

func (r *rentOfferResolver) Area(ctx context.Context, obj *prisma.RentOffer) (*prisma.Area, error) {
	return r.Prisma.RentOffer(prisma.RentOfferWhereUniqueInput{ID: &obj.ID}).Area().Exec(ctx)
}
