package resolvers

import (
	"bytes"
	"context"
	"fmt"
	"graphql_server/generated/models"
	"graphql_server/generated/prisma"
	"graphql_server/lib"
	"graphql_server/lib/dabucket"
	"io"
	"strings"

	"github.com/h2non/filetype"
	"github.com/h2non/filetype/types"
	"github.com/twmb/murmur3"
)

var (
	// ErrDefaultUpload represent the generic error for a failed file upload try
	ErrDefaultUpload = fmt.Errorf("Failed to upload file, try again later")
	// ErrAlreadyExist represent what it said
	ErrAlreadyExist = fmt.Errorf("File allready exist")
	// ErrBadType is explicit
	ErrBadType = fmt.Errorf("Invalid file type")
)

// FileUpload is a helper struct in an effort to keep this messy file clean ^^
type FileUpload struct {
	Nfile    dabucket.BucketFile
	Nasset   prisma.AssetCreateInput
	NuserDoc prisma.UserDocumentCreateInput
}

///////////////////////////
///
/// 📂 User Documents mutations
///

func streamToByte(stream io.Reader) []byte {
	buf := new(bytes.Buffer)
	buf.ReadFrom(stream)
	return buf.Bytes()
}

func getHash(tohash []byte) *string {
	Hash64 := murmur3.New64()
	if _, err := Hash64.Write(tohash); err != nil {
		lib.LogError("uploadAsset/gethash", err.Error())
		return nil
	}
	stringHash := fmt.Sprintf("%x", Hash64.Sum(nil))
	lib.LogInfo("uploadAsset/getHash", fmt.Sprintf("Got hash %s\n", stringHash))
	return &stringHash
}

func thisFileIstheSame(ctx context.Context, client *prisma.Client, filehash string) (bool, error) {
	return client.UserDocument(prisma.UserDocumentWhereUniqueInput{
		Hash: &filehash,
	}).Exists(ctx)
}

func thisFileExist(ctx context.Context, client *prisma.Client, filename string) (bool, error) {
	return client.UserDocument(prisma.UserDocumentWhereUniqueInput{
		Fname: &filename,
	}).Exists(ctx)
}

// Uploadasset save a user document to cloud bucket
func (r *MutationResolver) Uploadasset(ctx context.Context, input models.DocumentInput) (bool, error) {
	var err error
	var fileURL, fileHash *string
	var fileKind types.Type
	var fileexist, fileisame bool
	var usernameCtx = lib.ContextKey("username")

	username := ctx.Value(usernameCtx).(string)
	storageType := prisma.StorageTypeBucket
	if input.File.Size > lib.ServerConf.MaxSizeByte {
		logmsg := fmt.Sprintf("User %s trying to upload a big file of size %v", username, input.File.Size)
		lib.LogError("mutation/UploadAsset", logmsg)
		return false, fmt.Errorf("File to big")
	}
	// TODO: finish this implementation (maybe refacto with all the necessary checks)
	toBytes := streamToByte(input.File.File)
	fileKind, err = filetype.Match(toBytes)
	logmsg := fmt.Sprintf("Got file : %s of size %v and type %s\n",
		input.File.Filename,
		input.File.Size,
		fileKind.MIME.Value)
	lib.LogInfo("mutation/Uploadasset", logmsg)
	if fileKind == filetype.Unknown || !strings.Contains(lib.ServerConf.SupportedFileTypes, fileKind.MIME.Value) {
		lib.LogError("mutation/uploadasset", "Unknow or unsuported file type "+fileKind.MIME.Value)
		return false, ErrBadType
	}
	if fileHash = getHash(toBytes); fileHash == nil {
		lib.LogError("mutation/Uploadasset", "Failed to get file hash")
		return false, ErrDefaultUpload
	}
	// TODO: A Better code for the below op (create or update user doc)
	if fileexist, err = thisFileExist(ctx, r.Prisma, input.File.Filename); err != nil {
		lib.LogError("mutation/Uploadasset", err.Error())
		return false, ErrDefaultUpload
	}
	if fileisame, err = thisFileIstheSame(ctx, r.Prisma, *fileHash); err != nil {
		lib.LogError("mutation/Uploadasset", err.Error())
		return false, ErrDefaultUpload
	}
	if fileexist && fileisame {
		return false, ErrAlreadyExist
	}
	nfile := dabucket.BucketFile{
		Buffer: input.File.File,
		Fname:  input.File.Filename,
		Fpath:  username + "/documents/" + string(input.DocumentType) + "/" + input.File.Filename,
	}
	if fileURL, err = dabucket.CopyFile(ctx, nfile); err != nil {
		lib.LogError("mutation/Uploadasset", err.Error())
		return false, ErrDefaultUpload
	}
	nasset := prisma.AssetCreateInput{
		Url:         *fileURL,
		Name:        input.File.Filename,
		Type:        prisma.ContentTypeDocument,
		StorageType: &storageType,
	}
	nuserdoc := prisma.UserDocumentCreateInput{
		Hash:  *fileHash,
		Fname: nfile.Fpath,
		Label: prisma.LabelCreateOneInput{
			Create: &prisma.LabelCreateInput{
				Text: input.Label,
			},
		},
		Doctype: input.DocumentType,
		Asset: prisma.AssetCreateOneInput{
			Create: &nasset,
		},
	}
	if !fileexist {
		if _, err = r.Prisma.UpdateUser(prisma.UserUpdateParams{
			Where: prisma.UserWhereUniqueInput{Name: &username},
			Data: prisma.UserUpdateInput{
				Documents: &prisma.UserDocumentUpdateManyInput{
					Create: []prisma.UserDocumentCreateInput{
						nuserdoc,
					},
				},
			},
		}).Exec(ctx); err != nil {
			lib.LogError("mutation/Uploadasset", err.Error())
			return false, ErrDefaultUpload
		}
		logmsg = fmt.Sprintf("Sucessfully created document %s for user : %s\n", input.File.Filename, username)
		lib.LogInfo("mutation/Uploadasset", logmsg)
		return true, nil
	}
	if _, err = r.Prisma.UpdateUserDocument(prisma.UserDocumentUpdateParams{
		Where: prisma.UserDocumentWhereUniqueInput{Fname: &input.File.Filename},
		Data: prisma.UserDocumentUpdateInput{
			Doctype: &input.DocumentType,
			Label: &prisma.LabelUpdateOneRequiredInput{
				Update: &prisma.LabelUpdateDataInput{
					Text: &nuserdoc.Label.Create.Text,
				},
			},
			Asset: &prisma.AssetUpdateOneRequiredInput{
				Upsert: &prisma.AssetUpsertNestedInput{
					Create: nasset,
				},
			},
		},
	}).Exec(ctx); err != nil {
		lib.LogError("mutation/Uploadasset", err.Error())
		return false, ErrDefaultUpload
	}
	logmsg = fmt.Sprintf("Sucessfully created document %s for user : %s\n", input.File.Filename, username)
	lib.LogInfo("mutation/Uploadasset", logmsg)
	return true, nil
}
