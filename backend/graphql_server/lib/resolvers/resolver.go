package resolvers

import (
	"graphql_server/generated/prisma"
	"graphql_server/generated/server"
)

// Resolver is the root resolvers struct
type Resolver struct{ Prisma *prisma.Client }

// Mutation is the root mutations struct
func (r *Resolver) Mutation() server.MutationResolver {
	return &MutationResolver{r}
}

// User resolve User field
func (r *Resolver) User() server.UserResolver { return &UserResolver{r} }
