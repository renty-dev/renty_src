package resolvers

import (
	"context"
	"fmt"
	"graphql_server/generated/models"
	"graphql_server/generated/prisma"
	"graphql_server/lib"
	"graphql_server/lib/socialauth"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

const (
	localIP   = "127.0.0.1"
	devMockIP = "90.5.218.242"
)

// SocialLogin enable login from an OpendID provider (Google, Twitter, Microsoft ...)
func (r *queryResolver) SocialLogin(ctx context.Context, input models.SocialLoginInput) (*models.Auth, error) {
	var err error
	var token *string
	var resp *models.Auth
	var user *prisma.User
	var tokenData *jwt.Token
	var devices []prisma.Device
	var googleClaims *socialauth.GoogleClaims
	var userIPCtx = lib.ContextKey("userip")
	var userAgentCtx = lib.ContextKey("useragent")
	userip := ctx.Value(userIPCtx).(string)
	useragent := ctx.Value(userAgentCtx).(string)

	if tokenData, err = socialauth.GetTokenDatas(input.IDToken); err != nil {
		lib.LogError("mutation/SocialRegister", err.Error())
		return nil, fmt.Errorf("Bad token")
	}
	if googleClaims, err = socialauth.ParseGoogleClaims(tokenData); err != nil {
		return nil, fmt.Errorf("Bad token")
	}
	if exist, err := r.Prisma.SocialAuth(prisma.SocialAuthWhereUniqueInput{
		Uid: &googleClaims.Subject,
	}).Exists(ctx); err != nil || !exist {
		if err != nil {
			lib.LogError("resolvers/Login", err.Error())
		}
		return nil, fmt.Errorf("Please register first")
	}
	userQuery := prisma.UserWhereUniqueInput{
		Name: &googleClaims.Name,
	}
	if user, err = r.Prisma.User(userQuery).Exec(ctx); err != nil {
		return nil, fmt.Errorf("Invalid query")
	}
	if devices, err = r.Prisma.User(userQuery).Devices(nil).Exec(ctx); err != nil {
		lib.LogError("resolvers/Login", err.Error())
		return nil, fmt.Errorf("Invalid query")
	}
	// For dev mode
	if userip == localIP {
		userip = devMockIP
	}
	// Need to update user device
	if err := updateUserDevice(r.Prisma, devices, deviceUpdate{
		Userip:         userip,
		Useragent:      useragent,
		Username:       user.Name,
		NotificationID: input.NotificationID,
	}); err != nil {
		return nil, fmt.Errorf("Server error")
	}
	userInfos := user.Name + ":" + string(user.Role)
	loginType := "Auth"
	// If user have activated 2FA, a temp token is issued with otp audience
	exp := conf.SessionDuration
	if user.Totp != nil {
		exp = conf.TotpDuration
		loginType = "PreAuth"
	}
	token = lib.GetJwtString(exp, userInfos, user.Totp != nil)
	resp = &models.Auth{
		Token:     *token,
		Type:      loginType,
		Otp:       user.Totp != nil,
		CreatedAt: time.Now().Format(time.RFC3339),
	}
	if user.Totp != nil {
		resp.Validity = lib.ServerConf.TotpDuration.String()
	} else {
		resp.Validity = lib.ServerConf.SessionDuration.String()
	}
	return resp, nil
}

// Login return a jwt authorization token if (username or phone) and password match the ones in db
func (r *queryResolver) Login(ctx context.Context, input models.UserLoginInput) (*models.Auth, error) {
	var err error
	var resp *models.Auth
	var token *string
	var user *prisma.User
	var devices []prisma.Device
	var userIPCtx = lib.ContextKey("userip")
	var userAgentCtx = lib.ContextKey("useragent")
	userip := ctx.Value(userIPCtx).(string)
	useragent := ctx.Value(userAgentCtx).(string)

	if user, err = r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: input.Name,
	}).Exec(ctx); err != nil {
		lib.LogError("resolvers/Login", err.Error())
		return nil, fmt.Errorf("Invalid query")
	}
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(input.Password)); err != nil {
		lib.LogError("resolvers/Login", err.Error())
		return nil, fmt.Errorf("Invalid query")
	}
	if devices, err = r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: input.Name,
	}).Devices(nil).Exec(ctx); err != nil {
		lib.LogError("resolvers/Login", err.Error())
		return nil, fmt.Errorf("Invalid query")
	}
	// For dev mode
	if userip == localIP {
		userip = devMockIP
	}
	// Need to update user device
	if err := updateUserDevice(r.Prisma, devices, deviceUpdate{
		Userip:         userip,
		Useragent:      useragent,
		Username:       user.Name,
		NotificationID: input.NotificationID,
	}); err != nil {
		return nil, fmt.Errorf("Server error")
	}
	loginType := "Auth"
	// If user have activated 2FA, a temp token is issued with otp subject
	exp := conf.SessionDuration
	if user.Totp != nil {
		exp = conf.TotpDuration
		loginType = "PreAuth"
	}
	userInfos := user.Name + ":" + string(user.Role)
	token = lib.GetJwtString(exp, userInfos, user.Totp != nil)
	resp = &models.Auth{
		Token:     *token,
		Type:      loginType,
		Otp:       user.Totp != nil,
		CreatedAt: time.Now().Format(time.RFC3339),
	}
	if user.Totp != nil {
		resp.Validity = lib.ServerConf.TotpDuration.String()
	} else {
		resp.Validity = lib.ServerConf.SessionDuration.String()
	}
	return resp, nil
}
