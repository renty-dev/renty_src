package resolvers

import (
	"context"
	"graphql_server/generated/models"
	"graphql_server/generated/prisma"
	"graphql_server/lib"
	"time"
)

///////////////////////////
///
/// 📔 Offers mutations
///

// Addoffer allow an admin to save a rent offer in database
func (r *MutationResolver) Addoffer(ctx context.Context, input models.RentOfferInput) (*prisma.RentOffer, error) {
	var usernameCtx = lib.ContextKey("username")
	v := ctx.Value(usernameCtx).(string)
	return r.Prisma.CreateRentOffer(prisma.RentOfferCreateInput{
		AvailableFrom: time.Now().Format(time.RFC3339),
		CreatedBy: prisma.UserCreateOneInput{
			Connect: &prisma.UserWhereUniqueInput{Name: &v},
		},
		Assets: &prisma.AssetCreateManyInput{
			Create: []prisma.AssetCreateInput{{
				Type: prisma.ContentTypePhoto,
				Name: input.Pictures[0],
				Url:  input.Pictures[0],
			}}},
		Location: prisma.LocationCreateOneInput{
			Create: &prisma.LocationCreateInput{
				Latitude:  input.Location.Lat,
				Longitude: input.Location.Long,
				Metadatas: input.Location.Metas,
			},
		},
		Title: &prisma.LabelCreateManyInput{
			Create: []prisma.LabelCreateInput{
				{
					Text: input.Title,
				}},
		},
		Description: &prisma.LabelCreateManyInput{
			Create: []prisma.LabelCreateInput{
				{
					Text: input.Description,
				}},
		},
	}).Exec(ctx)
}
