package resolvers

import (
	"context"
	"graphql_server/generated/prisma"
)

func (r *queryResolver) CheckUsername(ctx context.Context, name string) (bool, error) {
	return r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: &name,
	}).Exists(ctx)
}
