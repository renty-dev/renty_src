package resolvers

import (
	"context"
	"graphql_server/generated/prisma"
	"graphql_server/generated/server"
)

// Subscription is the realtime feed root struct
func (r *Resolver) Subscription() server.SubscriptionResolver {
	return &SubscriptionResolver{r}
}

// SubscriptionResolver is resolving subscriptions (WIP)
type SubscriptionResolver struct{ *Resolver }

// Registrations is supposed to be the live registration feed for admin to see
func (r *SubscriptionResolver) Registrations(ctx context.Context) (<-chan *prisma.User, error) {
	panic("not implemented")
}
