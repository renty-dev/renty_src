package resolvers

import (
	"context"
	"fmt"
	"graphql_server/generated/prisma"
	"graphql_server/lib"

	"golang.org/x/crypto/bcrypt"
)

// UpdatePassword allow an authenticated  user to change his password
func (r *MutationResolver) UpdatePassword(ctx context.Context, old string, new string) (*bool, error) {
	var err error
	var resp = true
	var user *prisma.User
	var pwdHash []byte
	var usernameCtx = lib.ContextKey("username")
	v := ctx.Value(usernameCtx).(string)
	if user, err = r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: &v,
	}).Exec(ctx); err != nil {
		lib.LogError("mutation/UpdatePassword", err.Error())
		return nil, err
	}
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(old)); err != nil {
		lib.LogError("mutation/UpdatePassword", err.Error())
		return nil, err
	}
	if pwdHash, err = bcrypt.GenerateFromPassword([]byte(new), bcrypt.MinCost); err != nil {
		return nil, err
	}
	NewPassword := string(pwdHash)
	if _, err := r.Prisma.UpdateUser(prisma.UserUpdateParams{
		Data: prisma.UserUpdateInput{
			Password: &NewPassword,
		},
		Where: prisma.UserWhereUniqueInput{
			ID: &user.ID,
		},
	}).Exec(ctx); err != nil {
		lib.LogError("mutation/UpdatePassword", err.Error())
		resp = false
	}
	lib.LogInfo("mutation/UpdatePassword", fmt.Sprintf("%s successfully update his password", usernameCtx))
	return &resp, err
}
