package resolvers

import (
	"context"
	"fmt"
	"graphql_server/generated/models"
	"graphql_server/generated/prisma"
	"graphql_server/lib"
)

// Updatecontact update or set a user email or phone
func (r *MutationResolver) Updatecontact(ctx context.Context, input models.UpdateContactInput) (*prisma.User, error) {
	var err error
	var user *prisma.User
	var emailUpdate *prisma.EmailUpdateOneInput
	var phoneUpdate *prisma.PhoneNumberUpdateOneInput

	var usernameCtx = lib.ContextKey("username")
	v := ctx.Value(usernameCtx).(string)
	fmt.Printf("Got input : %+v\n", input)
	if input.Phone != nil {
		phoneUpdate = &prisma.PhoneNumberUpdateOneInput{
			Create: RegisterPhone(input.Phone),
		}
	}
	if input.Email != nil {
		emailUpdate = &prisma.EmailUpdateOneInput{
			Create: RegisterEmail(v, input.Email),
		}
	}
	if user, err = r.Prisma.UpdateUser(prisma.UserUpdateParams{
		Where: prisma.UserWhereUniqueInput{
			Name: &v,
		},
		Data: prisma.UserUpdateInput{
			Phone:     phoneUpdate,
			Email:     emailUpdate,
			Civility:  input.Civility,
			FirstName: input.FirstName,
			LastName:  input.LastName,
		},
	}).Exec(ctx); err != nil {
		return nil, fmt.Errorf("Invalid query")
	}
	return user, nil
}
