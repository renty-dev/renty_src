package resolvers

import (
	"context"
	"encoding/base64"
	"fmt"
	"graphql_server/generated/models"
	"graphql_server/generated/prisma"
	"graphql_server/generated/server"
	"graphql_server/lib"
	"graphql_server/lib/sendnotif"
	"strconv"
	"time"

	"github.com/skip2/go-qrcode"
	"github.com/xlzd/gotp"
)

type queryResolver struct{ *Resolver }

type deviceUpdate struct {
	Userip         string
	Useragent      string
	Username       string
	NotificationID *string
}

// Query is the main queries struct
func (r *Resolver) Query() server.QueryResolver {
	return &queryResolver{r}
}

var conf = lib.Config{}

func init() {
	conf.Load()
}

/// 👥 User & Authentication queries

func findDeviceByUA(userdevices []prisma.Device, useragent string) *prisma.Device {
	for index, device := range userdevices {
		if device.UserAgent == useragent {
			return &userdevices[index]
		}
	}
	return nil
}
func updateUserDevice(cli *prisma.Client, userDevices []prisma.Device, update deviceUpdate) error {
	updated := false
	var err error
	var deviceObject *prisma.DeviceCreateInput
	var deviceToUpdate = findDeviceByUA(userDevices, update.Useragent)
	if deviceObject, err = RegisterDevice(update.Userip, update.Useragent, update.NotificationID); err != nil {
		return err
	}
	if deviceToUpdate != nil {
		update := prisma.DeviceUpdateParams{
			Where: prisma.DeviceWhereUniqueInput{
				ID: &deviceToUpdate.ID,
			},
			Data: prisma.DeviceUpdateInput{
				Ips: &prisma.DeviceUpdateipsInput{
					Set: append(deviceToUpdate.Ips, update.Userip),
				},
				Visits: &prisma.DeviceUpdatevisitsInput{
					Set: append(deviceToUpdate.Visits, time.Now().Format(time.RFC3339)),
				},
				NotificationId: update.NotificationID,
				GeoIp: &prisma.LocationUpdateManyInput{
					Create: deviceObject.GeoIp.Create,
				},
			},
		}
		if _, err := cli.UpdateDevice(update).Exec(context.Background()); err != nil {
			return err
		}
		updated = true
	}
	if !updated {
		if _, err := cli.UpdateUser(prisma.UserUpdateParams{
			Where: prisma.UserWhereUniqueInput{Name: &update.Username},
			Data: prisma.UserUpdateInput{
				Devices: &prisma.DeviceUpdateManyInput{
					Create: []prisma.DeviceCreateInput{
						*deviceObject,
					},
				},
			},
		}).Exec(context.Background()); err != nil {
			return err
		}
	}
	return nil
}

// Confirm return the real authorization token against the temporary code for user who have activated 2FA
func (r *queryResolver) Confirm(ctx context.Context, code int) (*models.Auth, error) {
	var err error
	var user *prisma.User
	var sessionExp time.Duration
	var usernameCtx = lib.ContextKey("username")
	username := ctx.Value(usernameCtx).(string)
	user, err = r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: &username,
	}).Exec(ctx)
	totp := gotp.NewDefaultTOTP(*user.Totp)
	currOtp, _ := strconv.Atoi(totp.Now())
	if check := (currOtp == code); !check {
		return nil, fmt.Errorf("Invalid otp")
	}
	return &models.Auth{
		Token: *lib.GetJwtString(sessionExp, username, false),
		Type:  "Bearer",
		Otp:   false,
	}, err
}

// Mydocuments return the user uploaded documents
func (r *queryResolver) Mydocuments(ctx context.Context) ([]prisma.UserDocument, error) {
	var err error
	var docs []prisma.UserDocument
	var usernameCtx = lib.ContextKey("username")
	v := ctx.Value(usernameCtx)
	if v != nil {
		str := v.(string)
		if docs, err = r.Prisma.User(prisma.UserWhereUniqueInput{Name: &str}).Documents(nil).Exec(ctx); err != nil {
			return nil, err
		}
		return docs, err
	}
	return nil, fmt.Errorf("Invalid query")
}

// User return a user profile fields
func (r *queryResolver) User(ctx context.Context) (*prisma.User, error) {
	var err error
	var user *prisma.User
	var usernameCtx = lib.ContextKey("username")
	v := ctx.Value(usernameCtx)
	if v != nil {
		str := v.(string)
		if user, err = r.Prisma.User(prisma.UserWhereUniqueInput{Name: &str}).Exec(ctx); err != nil {
			return nil, err
		}
		return user, err
	}
	return nil, fmt.Errorf("Invalid query")
}

// Add2fa return the link of the qrcode in base64 for user who want to have a 2 factor authentication
func (r *queryResolver) Add2fa(ctx context.Context) (*models.Totp, error) {
	var err error
	nameCtx := lib.ContextKey("username")
	ctxval := ctx.Value(nameCtx)
	// Getting User
	if ctxval != nil {
		var png []byte
		var baseEncoded string
		var randomSeed = ""
		var secretLength = 16
		var user *prisma.User
		username := ctxval.(string)
		if user, err = r.Prisma.User(prisma.UserWhereUniqueInput{
			Name: &username,
		}).Exec(ctx); err != nil {
			lib.LogError("resolvers/Add2fa", err.Error())
			return nil, fmt.Errorf("Invalid query")
		}
		if user.Totp != nil {
			return nil, fmt.Errorf("You have already activated the 2FA option")
		}
		// Generate random Secret seed
		randomSeed = gotp.RandomSecret(secretLength)
		// Getting totp:// style URL TODO: Put the below string('renty-dev') in env var
		otp := gotp.NewDefaultTOTP(randomSeed).ProvisioningUri(user.Name, "Renty-dev")
		// Generating a png QRCode
		if png, err = qrcode.Encode(otp, qrcode.Medium, 256); err != nil {
			return nil, err
		}
		// Converting it to base64 string
		baseEncoded = base64.StdEncoding.EncodeToString(png)
		// Save user 2FA variable
		if _, err := r.Prisma.UpdateUser(prisma.UserUpdateParams{
			Data: prisma.UserUpdateInput{
				Totp: &randomSeed,
			},
			Where: prisma.UserWhereUniqueInput{
				ID: &user.ID,
			},
		}).Exec(ctx); err != nil {
			return nil, err
		}
		// & finally returning all infos :)
		return &models.Totp{
			URL:    otp,
			Qrcode: baseEncoded,
		}, err
	}
	return nil, fmt.Errorf("Invalid query")
}

// Userbyname allow the admin to look for a user details knowing it's username
func (r *queryResolver) Userbyname(ctx context.Context, name string) (*prisma.User, error) {
	return r.Prisma.User(prisma.UserWhereUniqueInput{Name: &name}).Exec(ctx)
}

// Notify allow an admin to send a firebase notification to a user
func (r *queryResolver) Notify(ctx context.Context, input models.NotificationInput) (*models.NotifyReport, error) {
	var token string
	var count = 0
	var err error
	var targetUsrDevices []prisma.Device
	report := models.NotifyReport{}
	notif := sendnotif.PushNotif{
		Title:    input.Title,
		Body:     input.Body,
		AssetURL: input.AssetURL,
	}

	if targetUsrDevices, err = r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: &input.Username,
	}).Devices(nil).Exec(ctx); err != nil {
		return nil, err
	}
	// Send a notification to every user device who got a firebase token (DESKTOP or MOBILE)
	for _, device := range targetUsrDevices {
		sent := false
		if device.NotificationId != nil {
			token = *device.NotificationId
			notif.To = token
			if input.PreferedTarget != nil && device.DeviceType == *input.PreferedTarget {
				sendnotif.SendFCMNotif(notif)
				sent = true
			} else if input.PreferedTarget == nil {
				sendnotif.SendFCMNotif(notif)
				sent = true
			}
			if sent {
				report.Devices = append(report.Devices, device.DeviceType)
				count++
			}
		}
	}
	report.Sent = count
	return &report, nil
}

/// 📔 Offers Queries :

// Offers present all the public offers Renty have in db
func (r *queryResolver) Offers(ctx context.Context) ([]prisma.RentOffer, error) {
	var isprivate = false
	return r.Prisma.RentOffers(&prisma.RentOffersParams{
		Where: &prisma.RentOfferWhereInput{
			IsPrivate: &isprivate,
		}}).Exec(ctx)
}

// Myoffers return all the offers shared with or created by the requesting user
func (r *queryResolver) Myoffers(ctx context.Context) ([]prisma.RentOffer, error) {
	var usernameCtx = lib.ContextKey("username")
	v := ctx.Value(usernameCtx)
	if v != nil {
		str := v.(string)
		return r.Prisma.RentOffers(&prisma.RentOffersParams{
			Where: &prisma.RentOfferWhereInput{
				CreatedBy: &prisma.UserWhereInput{
					Name: &str,
				},
				Or: []prisma.RentOfferWhereInput{{
					SharedWithEvery: &prisma.UserWhereInput{Name: &str},
				},
				},
			}}).Exec(ctx)
	}
	return nil, fmt.Errorf("Invalid query")
}

func (r *queryResolver) LikeOffer(ctx context.Context, offerID string, value bool) (bool, error) {
	var usernameCtx = lib.ContextKey("username")
	v := ctx.Value(usernameCtx).(string)
	if _, err := r.Prisma.UpdateUser(prisma.UserUpdateParams{
		Where: prisma.UserWhereUniqueInput{Name: &v},
		Data: prisma.UserUpdateInput{
			LikedOffers: &prisma.RentOfferUpdateManyInput{
				Connect: []prisma.RentOfferWhereUniqueInput{
					{
						ID: &offerID,
					},
				},
			},
		},
	}).Exec(ctx); err != nil {
		return false, fmt.Errorf("Bad offer id")
	}
	return true, nil
}
func (r *queryResolver) Checkmein(ctx context.Context, offer string) (*bool, error) {
	rt := false
	return &rt, nil
}
