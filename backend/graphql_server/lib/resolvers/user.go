package resolvers

import (
	"context"
	"graphql_server/generated/prisma"
)

// UserResolver is resolving the user type
type UserResolver struct{ *Resolver }

// Phone is resolving the nested phone field in user schema
func (r *UserResolver) Phone(ctx context.Context, obj *prisma.User) (*prisma.PhoneNumber, error) {
	res, err := r.Prisma.User(prisma.UserWhereUniqueInput{ID: &obj.ID}).Phone().Exec(ctx)
	if err != nil {
		res = nil
	}
	return res, nil
}

// Devices is resolving the nested devices field in user schema
func (r *UserResolver) Devices(ctx context.Context, obj *prisma.User) ([]prisma.Device, error) {
	return r.Prisma.User(prisma.UserWhereUniqueInput{ID: &obj.ID}).Devices(nil).Exec(ctx)
}

// Email is resolving the nested email field in user schema
func (r *UserResolver) Email(ctx context.Context, obj *prisma.User) (*prisma.Email, error) {
	res, err := r.Prisma.User(prisma.UserWhereUniqueInput{ID: &obj.ID}).Email().Exec(ctx)
	if err != nil {
		res = nil
	}
	return res, nil
}

// LikedOffers is resolving the nested liked rent offers array in user schema
func (r *UserResolver) LikedOffers(ctx context.Context, obj *prisma.User) ([]prisma.RentOffer, error) {
	res, err := r.Prisma.User(prisma.UserWhereUniqueInput{ID: &obj.ID}).LikedOffers(nil).Exec(ctx)
	if err != nil {
		res = nil
	}
	return res, nil
}
