package resolvers

import (
	"context"
	"fmt"
	"graphql_server/generated/prisma"
	"graphql_server/lib/dabucket"
)

// Deleteasset allow an user to remove one his userdoc
func (r *MutationResolver) Deleteasset(ctx context.Context, hash string) (bool, error) {
	var err error
	var asset *prisma.Asset

	// Getting file url
	if asset, err = r.Prisma.UserDocument(prisma.UserDocumentWhereUniqueInput{Hash: &hash}).Asset().Exec(ctx); err != nil {
		return false, fmt.Errorf("Failed to remove document")
	}
	// Removing file from bucket
	if err = dabucket.DeleteFile(ctx, asset.Url); err != nil {
		return false, fmt.Errorf("Failed to remove document")
	}
	// Removing reference from database
	if _, err = r.Prisma.DeleteUserDocument(prisma.UserDocumentWhereUniqueInput{Hash: &hash}).Exec(ctx); err != nil {
		return false, fmt.Errorf("Failed to remove document")
	}
	return true, nil
}
