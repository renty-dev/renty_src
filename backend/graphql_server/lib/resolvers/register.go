package resolvers

import (
	"context"
	"fmt"
	"graphql_server/generated/models"
	"graphql_server/generated/prisma"
	"graphql_server/lib"
	"graphql_server/lib/socialauth"
	"math/rand"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/rs/xid"
	"golang.org/x/crypto/bcrypt"
)

func getPseudoRandomCost() int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn((20 - bcrypt.MinCost) + bcrypt.MinCost)
}

// Socialregister save a new renty user social id and a forged password hash in database
func (r *MutationResolver) Socialregister(ctx context.Context, input *models.SocialLoginInput) (*prisma.User, error) {
	var err error
	var pwdHash []byte
	var tokenData *jwt.Token
	var newDevice *prisma.DeviceCreateInput
	var googleClaims *socialauth.GoogleClaims
	var useripCtx = lib.ContextKey("userip")
	var userAgentCtx = lib.ContextKey("useragent")
	var newPassword = xid.New().String()

	userip := ctx.Value(useripCtx).(string)
	useragent := ctx.Value(userAgentCtx).(string)

	if tokenData, err = socialauth.GetTokenDatas(input.IDToken); err != nil {
		lib.LogError("mutation/SocialRegister", err.Error())
		return nil, fmt.Errorf("Bad token")
	}
	if googleClaims, err = socialauth.ParseGoogleClaims(tokenData); err != nil {
		lib.LogError("mutation/SocialRegister", err.Error())
		return nil, fmt.Errorf("Bad token")
	}
	if exist, err := r.Prisma.SocialAuth(prisma.SocialAuthWhereUniqueInput{
		Uid: &googleClaims.Subject,
	}).Exists(ctx); err != nil || exist {
		if err != nil {
			lib.LogError("mutation/SocialRegister", err.Error())
		}
		return nil, fmt.Errorf("Allready registered, please login")
	}
	if pwdHash, err = bcrypt.GenerateFromPassword([]byte(newPassword), getPseudoRandomCost()); err != nil {
		lib.LogError("mutation/SocialRegister", err.Error())
		return nil, fmt.Errorf("Server error")
	}
	logMsg := fmt.Sprintf("%s trying to register (with new password : %s)", googleClaims.Name, newPassword)
	lib.LogInfo("mutation/Register", logMsg)
	if newDevice, err = RegisterDevice(userip, useragent, input.NotificationID); err != nil {
		lib.LogError("mutation/SocialRegister", err.Error())
		return nil, fmt.Errorf("Server error")
	}
	return r.Prisma.CreateUser(prisma.UserCreateInput{
		Name:      googleClaims.Name,
		FirstName: &googleClaims.GivenName,
		Password:  string(pwdHash),
		SocialAuth: &prisma.SocialAuthCreateManyInput{
			Create: []prisma.SocialAuthCreateInput{
				{
					Uid:      googleClaims.Subject,
					Provider: input.Provider,
				},
			},
		},
		Email: &prisma.EmailCreateOneInput{
			Create: RegisterEmail(googleClaims.Name, &googleClaims.Email),
		},
		Devices: &prisma.DeviceCreateManyInput{
			Create: []prisma.DeviceCreateInput{
				*newDevice,
			},
		},
	}).Exec(ctx)
}

// Register save a new renty user name and password hash in database
func (r *MutationResolver) Register(ctx context.Context, input *models.UserRegisterInput) (*prisma.User, error) {
	var err error
	var pwdHash []byte
	var newDevice *prisma.DeviceCreateInput
	var useripCtx = lib.ContextKey("userip")
	var userAgentCtx = lib.ContextKey("useragent")

	userAgentStr := ctx.Value(userAgentCtx).(string)
	userIPStr := ctx.Value(useripCtx).(string)

	if exist, err := r.Prisma.User(prisma.UserWhereUniqueInput{Name: &input.Name}).Exists(ctx); err != nil || exist {
		if err != nil {
			lib.LogError("mutation/Register", err.Error())
		}
		return nil, fmt.Errorf("Name allready taken")
	}
	if pwdHash, err = bcrypt.GenerateFromPassword([]byte(input.Password), getPseudoRandomCost()); err != nil {
		lib.LogError("mutation/Register", err.Error())
		return nil, err
	}
	lib.LogInfo("mutation/Register", fmt.Sprintf("%s trying to register", input.Name))
	if newDevice, err = RegisterDevice(userIPStr, userAgentStr, input.NotificationID); err != nil {
		lib.LogError("mutation/Register", err.Error())
		return nil, err
	}
	return r.Prisma.CreateUser(prisma.UserCreateInput{
		Name:     input.Name,
		Civility: input.Civility,
		Password: string(pwdHash),
		Email: &prisma.EmailCreateOneInput{
			Create: RegisterEmail(input.Name, input.Email),
		},
		Phone: &prisma.PhoneNumberCreateOneInput{
			Create: RegisterPhone(input.Phone),
		},
		Devices: &prisma.DeviceCreateManyInput{
			Create: []prisma.DeviceCreateInput{
				*newDevice,
			},
		},
	}).Exec(ctx)
}
