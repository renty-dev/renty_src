package lib

import (
	"os"
	"strconv"
	"time"
)

// Config represent the graphql_server configuration variables
type Config struct {
	Stage                  string `env:"STAGE"`
	Release                string `env:"RELEASE"`
	Port                   string `env:"PORT"`
	Host                   string `env:"HOST"`
	URL                    string `env:"URL"`
	JwtIssuer              string `env:"JWT_ISSUER"`
	JwtSigningKey          string `env:"SIGN_KEY"`
	TotpExpiration         string `env:"TOTP_EXP"`
	SessionExpiration      string `env:"SESSION_EXP"`
	SMTPHost               string `env:"SMTP_HOST"`
	SMTPPort               string `env:"SMTP_PORT"`
	SMTPUser               string `env:"SMTP_USER"`
	SMTPPassword           string `env:"SMTP_PASS"`
	SMTPFromField          string `env:"SMTP_FROM"`
	MailDomain             string `env:"MAIL_DOMAIN"`
	SupportEmail           string `env:"SUPPORT_EMAIL"`
	FCMJSONConf            string `env:"GOOGLE_APPLICATION_CREDENTIAL"`
	B2BUCKETAccount        string `env:"B2_ACCOUNT"`
	B2BUCKETKey            string `env:"B2_KEY"`
	B2BUCKETName           string `env:"B2_BUCKETNAME"`
	MailConfirmRedirectURL string `env:"MAIL_REDIRECT"`
	GeoIPApiURL            string `env:"GEOIP_URL"`
	SentryDSN              string `env:"SENTRY_DSN"`
	SupportedFileTypes     string `env:"SUPPORTED_MIMES"`
	MaxUploadFileSize      string `env:"MAX_UPLOADSIZE"`
	MaxSizeByte            int64
	TotpDuration           time.Duration
	SessionDuration        time.Duration
}

// ServerConf is the exported config object singleton
var ServerConf = Config{}

func init() {
	ServerConf.Load()
}

// Load config options from environnement variables
func (c *Config) Load() {
	var err error
	c.Stage = GetDefVal("STAGE", "dev")
	c.Release = GetDefVal("RELEASE", "v0.1.11")
	c.Port = GetDefVal("PORT", "4000")
	c.Host = GetDefVal("HOST", "localhost")
	c.URL = GetDefVal("URL", "http://localhost:4000")
	c.JwtIssuer = GetDefVal("JWT_ISSUER", "renty-dev")
	c.JwtSigningKey = GetDefVal("SIGN_KEY", "SUPERSECRETTOBECHANGED")
	c.TotpExpiration = GetDefVal("TOTP_EXP", "15m")
	c.SessionExpiration = GetDefVal("SESSION_EXP", "12h")
	c.SMTPHost = GetDefVal("SMTP_HOST", "smtp.mailtrap.io")
	c.SMTPPort = GetDefVal("SMTP_PORT", "2525")
	c.SMTPUser = GetDefVal("SMTP_USER", "b31e79a68f61f3")
	c.SMTPPassword = GetDefVal("SMTP_PASS", "c86cedf162bad4")
	c.SMTPFromField = GetDefVal("SMTP_FROM", "Renty <no-reply@therentyapp.xyz>")
	c.MailDomain = GetDefVal("MAIL_DOMAIN", "therentyapp.xyz")
	c.SupportEmail = GetDefVal("SUPPORT_EMAIL", "support@therentyapp.xyz")
	c.FCMJSONConf = GetDefVal("GOOGLE_APPLICATION_CREDENTIAL", "x")
	c.B2BUCKETAccount = GetDefVal("B2_ACCOUNT", "x")
	c.B2BUCKETKey = GetDefVal("B2_KEY", "x")
	c.B2BUCKETName = GetDefVal("B2_BUCKETNAME", "renty-user-docs")
	c.MailConfirmRedirectURL = GetDefVal("MAIL_REDIRECT", "http://localhost:3000")
	c.GeoIPApiURL = GetDefVal("GEOIP_API", "https://freegeoip.app/json")
	c.SentryDSN = GetDefVal("SENTRY_DSN", "https://67f58ee98e4e4b76a50ffc280a8eaaf1@sentry.io/5176087")
	c.MaxUploadFileSize = GetDefVal("MAX_UPLOADSIZE", "20971520")
	c.SupportedFileTypes = GetDefVal("SUPPORTED_MIMES", `
		image/jpeg
		image/png
		application/pdf
		application/msword
	`,
	)
	if c.MaxSizeByte, err = strconv.ParseInt(c.MaxUploadFileSize, 10, 64); err != nil {
		LogError("lib/Config", err.Error())
		os.Exit(1)
	}
	if c.TotpDuration, err = time.ParseDuration(c.TotpExpiration); err != nil {
		LogError("lib/Config", err.Error())
		os.Exit(1)
	}
	if c.SessionDuration, err = time.ParseDuration(c.SessionExpiration); err != nil {
		LogError("lib/Config", err.Error())
		os.Exit(1)
	}
}
