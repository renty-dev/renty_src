package sendnotif

import (
	"graphql_server/lib"
	"os"

	"golang.org/x/net/context"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"

	"google.golang.org/api/option"
)

var app *firebase.App

// PushNotif is a wrapper around fcm notification
type PushNotif struct {
	To       string
	Title    string
	Body     string
	AssetURL *string
}

func init() {
	var err error
	var opt option.ClientOption
	jsonEnv := lib.ServerConf.FCMJSONConf
	if jsonEnv != "x" {
		opt = option.WithCredentialsJSON([]byte(jsonEnv))
	} else {
		opt = option.WithCredentialsFile("lib/sendnotif/renty-firebase-firebase-adminsdk-pj8sh-db49fb9abb.json")
	}
	if app, err = firebase.NewApp(context.Background(), nil, opt); err != nil {
		lib.LogError("sendnotif/init", err.Error())
		os.Exit(1)
	}
}

// SendFCMNotif just does what it say
func SendFCMNotif(notification PushNotif) {
	var err error
	var res string
	var client *messaging.Client
	var urlString = ""
	if notification.AssetURL != nil {
		urlString = *notification.AssetURL
	}
	// TODO: Update datas and message templates
	var testMessage = &messaging.Message{
		Data: map[string]string{"foo": "bar"},
		Notification: &messaging.Notification{
			Title: notification.Title,
			Body:  notification.Body,
		},
		Android: &messaging.AndroidConfig{
			Data:     map[string]string{"foo": "bar"},
			Priority: "high",
			Notification: &messaging.AndroidNotification{
				Sound:               "default",
				Sticky:              false,
				Visibility:          1,
				ImageURL:            urlString,
				VibrateTimingMillis: []int64{1000, 2000, 1000, 2000},
			},
		},
		Webpush: &messaging.WebpushConfig{
			Data:         map[string]string{"foo": "bar"},
			Notification: &messaging.WebpushNotification{},
		},
		APNS:  &messaging.APNSConfig{},
		Token: notification.To,
	}
	ctx := context.Background()
	if client, err = app.Messaging(ctx); err != nil {
		lib.LogError("sendnotif/SendFCMNotif", err.Error())
		return
	}
	if res, err = client.Send(ctx, testMessage); err != nil {
		lib.LogError("sendnotif/SendFCMNotif", err.Error())
		return
	}
	lib.LogInfo("sendnotif/SendFCMNotif", res)
}
