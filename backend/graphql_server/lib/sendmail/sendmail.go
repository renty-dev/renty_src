// Package sendmail is originally taken from
// (here)[https://github.com/badoux/checkmail/blob/master/checkmail.go]
// All smtp archiecture learning credits goes to the original author (badoux)
package sendmail

import (
	"bytes"
	"errors"
	"fmt"
	"graphql_server/lib"
	"log"
	"net/smtp"
	"regexp"
	"runtime"
	"strings"
	"text/template"
	"time"

	"github.com/dgryski/trifles/uuid"
)

// SMTPAuth represent the smtp server credentials
type SMTPAuth struct {
	Host     string `toml:"smtp_host"`
	Port     string `toml:"smtp_port"`
	UserName string `toml:"smtp_user"`
	Password string `toml:"smtp_pass"`
}

// SMTPError represent a smtp lookup error
type SMTPError struct {
	Err error
}

// Email represent a mail to send
type Email struct {
	To      []string
	From    string
	Subject string
	Message string
}

const (
	// MailRegexCheck ...
	MailRegexCheck = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
)

// ValidateFormat check email validity
func ValidateFormat(email string) error {
	emailRegexp := regexp.MustCompile(MailRegexCheck)
	if !emailRegexp.MatchString(email) {
		return fmt.Errorf("Invalid email format")
	}
	return nil
}

func (e SMTPError) Error() string {
	return e.Err.Error()
}

// Code return SMTPError code
func (e SMTPError) Code() string {
	return e.Err.Error()[0:3]
}

// NewSMTPError create a new SMTP Error
func NewSMTPError(err error, details *string) SMTPError {
	rt := SMTPError{
		Err: err,
	}
	if details != nil {
		rt.Err = fmt.Errorf("%s %s", err.Error(), *details)
	}
	return rt
}

const forceDisconnectAfter = time.Second * 5

var (
	// ErrUnresolvableHost return invalid host error msg
	ErrUnresolvableHost = errors.New("unresolvable host")
)

var smtpAuth = SMTPAuth{}

// Init setup smtp config
func init() {
	smtpAuth.Host = lib.ServerConf.SMTPHost
	smtpAuth.Port = lib.ServerConf.SMTPPort
	smtpAuth.UserName = lib.ServerConf.SMTPUser
	smtpAuth.Password = lib.ServerConf.SMTPPassword
	if smtpAuth.Host == "" || smtpAuth.Port == "" ||
		smtpAuth.UserName == "" || smtpAuth.Password == "" {
		err := fmt.Sprintf("Cannot start without SMTP envaars")
		lib.LogError("sendmail/init", err)
		log.Fatal(err)
	}
}

// catchPanic check and recover from error in mail sending
func catchPanic(err *error, functionName string) {
	if r := recover(); r != nil {
		logmsg := fmt.Sprintf("%s : PANIC Defered : %v\n", functionName, r)
		lib.LogError("sendmail/catchPanic", logmsg)
		// Capture the stack trace
		buf := make([]byte, 10000)
		runtime.Stack(buf, false)
		if err != nil {
			*err = fmt.Errorf("%v", r)
		}
	} else if err != nil && *err != nil {
		errval := *err
		lib.LogError("sendmail/catchPanic", errval.Error())
		// Capture the stack trace
		buf := make([]byte, 10000)
		runtime.Stack(buf, false)
		lib.LogError("sendmail/catchPanic", string(buf))
	}
}

// GetEmailValidationTemplate return an html template string filled with the required variables
func GetEmailValidationTemplate(username string, confirmationLink string, supportEmail string) *string {
	var err error
	var t *template.Template

	// TODO: put this template in database (get this shit out from server code)
	if t, err = template.ParseFiles("mail_templates/confirm_email.html"); err != nil {
		lib.LogError("getTemplate1", err.Error())
		return nil
	}
	data := struct {
		Username     string
		ConfirmLink  string
		SupportEmail string
	}{
		Username:     username,
		ConfirmLink:  confirmationLink,
		SupportEmail: supportEmail,
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, data); err != nil {
		lib.LogError("getTemplate", err.Error())
		return nil
	}

	result := tpl.String()
	return &result
}

// SendEmail send and email with go smtp package
func SendEmail(email Email) (err error) {
	const CRLF = "\r\n"
	const rfc2822 = "Mon, 02 Jan 2006 15:04:05 -0700"
	defer catchPanic(&err, "SendEmail")
	auth := smtp.PlainAuth("", smtpAuth.UserName, smtpAuth.Password, smtpAuth.Host)
	mime := "MIME-version: 1.0;" + CRLF
	tomsg := "To: " + strings.Join(email.To, ",") + CRLF
	frommsg := "From: " + email.From + CRLF
	subjectmsg := "Subject: " + email.Subject + CRLF
	date := "Date: " + time.Now().Format(rfc2822) + CRLF
	messageID := "Message-ID: <" + uuid.UUIDv4() + "@" + lib.ServerConf.MailDomain + ">" + CRLF
	contentTransferEncoding := "Content-Transfer-Encoding: 8bit" + CRLF
	contentType := "Content-Type: text/html; charset=\"UTF-8\";" + CRLF
	fullBody := fmt.Sprintf("%s%s%s%s%s%s%s%s%s", date, tomsg, frommsg,
		subjectmsg,
		messageID,
		mime,
		contentTransferEncoding,
		contentType,
		email.Message,
	)
	logmsg := fmt.Sprintf("Will send mail via %s:%s from %s %s\n",
		smtpAuth.Host,
		smtpAuth.Port,
		email.From,
		tomsg,
	)
	lib.LogInfo("SendEmail", logmsg)
	return smtp.SendMail(
		smtpAuth.Host+":"+smtpAuth.Port,
		auth,
		smtpAuth.UserName,
		email.To,
		[]byte(fullBody))
}
