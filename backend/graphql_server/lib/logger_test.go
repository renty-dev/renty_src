package lib

import (
	"strings"
	"testing"
)

func TestLogInfo(t *testing.T) {
	rt := LogInfo("loggerTest", "this is an info message")
	if !strings.Contains(rt, " 💡 Info(loggerTest): this is an info message") {
		t.Errorf("LogInfo failed to print log on stdout, got %s\n", rt)
	}
}

func TestLogError(t *testing.T) {
	rt := LogError("loggerTest", "this is an error message")
	if !strings.Contains(rt, " 🚨  Error(loggerTest): this is an error message") {
		t.Errorf("LogError failed to print log on stderr, got %s\n", rt)
	}
}
