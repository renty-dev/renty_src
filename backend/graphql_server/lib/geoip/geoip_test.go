package geoip

import (
	"fmt"
	"log"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	if err := os.Setenv("STAGE", "test"); err != nil {
		log.Fatalf("Error setting needed env var %s\n", err.Error())
	}
	os.Exit(m.Run())
}

func TestGetGeoip(t *testing.T) {
	var res *GeoIP
	testvalidaddress := "8.8.8.8"
	testbadaddress := "127.0.0.1:52416"
	if res = GetGeoip(testbadaddress); res != nil {
		fmt.Printf("Got %+v\n", res)
		t.Errorf("GetGeoip failed to catch error on address %s\n", testbadaddress)
	}
	if res = GetGeoip(testvalidaddress); res == nil {
		t.Errorf("GetGeoip failed to get infos on address %s\n", testvalidaddress)
	}
	fmt.Printf("Got resp : %+v\n", res)
	if res != nil && res.CountryCode != "US" {
		t.Errorf("GetGeoip failed to get good infos on address %s want US got %s\n", testvalidaddress, res.CountryCode)
	}
}
