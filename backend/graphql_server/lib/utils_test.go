package lib

import (
	"context"
	"testing"
	"time"
)

func TestGetJwtString(t *testing.T) {
	var dummysigned = GetJwtString(time.Duration(300), "subject", false)
	if dummysigned == nil {
		t.Error("GetJwtString return a null string when otp = false")
	}
	dummysigned = GetJwtString(time.Duration(300), "subject", true)
	if dummysigned == nil {
		t.Error("GetJwtString return a null string when otp = true")
	}
}

func TestContextKey(t *testing.T) {
	testCtx := ContextKey("test")
	ctx := context.Background()
	ctx = context.WithValue(ctx, testCtx, "value")
	v := ctx.Value(testCtx).(string)
	if v != "value" {
		t.Errorf("Failed to set ContextKey string want 'value' got '%s'", v)
	}
}
