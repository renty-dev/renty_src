package lib

import "testing"

func TestGetDefVal(t *testing.T) {
	var dummy = GetDefVal("impossibletohaveenvaar", "value")
	if dummy != "value" {
		t.Errorf("GetDefVal failed to set default value want 'value' got '%s'", dummy)
	}
}
