module graphql_server

go 1.14

require (
	firebase.google.com/go v3.12.1+incompatible
	github.com/99designs/gqlgen v0.11.3
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dgryski/trifles v0.0.0-20200323201526-dd97f9abfb48
	github.com/getsentry/sentry-go v0.6.0
	github.com/gin-gonic/gin v1.6.2
	github.com/golang/protobuf v1.4.0 // indirect
	github.com/gorilla/websocket v1.4.2
	github.com/h2non/filetype v1.0.12
	github.com/kurin/blazer v0.5.3
	github.com/machinebox/graphql v0.2.2
	github.com/matryer/is v1.3.0 // indirect
	github.com/mileusna/useragent v0.0.0-20200130135054-eb80d80699e8
	github.com/nyaruka/phonenumbers v1.0.55
	github.com/prisma/prisma-client-lib-go v0.0.0-20181017161110-68a1f9908416
	github.com/rs/xid v1.2.1
	github.com/skip2/go-qrcode v0.0.0-20191027152451-9434209cb086
	github.com/twmb/murmur3 v1.1.3
	github.com/vektah/gqlparser/v2 v2.0.1
	github.com/xlzd/gotp v0.0.0-20181030022105-c8557ba2c119
	golang.org/x/crypto v0.0.0-20200423211502-4bdfaf469ed5
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0
	google.golang.org/api v0.22.0
)
