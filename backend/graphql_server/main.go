package main

import (
	"graphql_server/lib/middleware"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.Use(middleware.SetContext())
	r.Use(middleware.CORSMiddleware())
	r.GET("/checkemail/:token", middleware.ConfirmEmail())
	r.POST("/query", middleware.GraphqlHandler())
	r.GET("/", middleware.PlaygroundHandler())
	r.Run()
}
