# Renty Backend stack
[![ForTheBadge built-by-developers](https://ForTheBadge.com/images/badges/built-by-developers.svg)](https://GitHub.com/Naereen/)
[![ForTheBadge powered-by-electricity](https://ForTheBadge.com/images/badges/powered-by-electricity.svg)](https://ForTheBadge.com)
[![ForTheBadge uses-badges](https://ForTheBadge.com/images/badges/uses-badges.svg)](https://ForTheBadge.com)
 

## Quickstart / Prisma Server :

- Read the [docker-compose](./docker-compose.yml) file. spoiler : u'll find Prisma server and Postgresql images.

- Start it up in background : `docker-compose up -d`.

- Create a file like `touch .dev.env` in the present directory .

- Have a look at the [secrets](https://gitlab.com/renty-dev/renty_src/-/wikis/secrets/home) page on the wiki for guidance on the variables and values to put in.

- Once filled, populate your database and generate gitignored code with `make dbscheme` .

- Fill the databse with the default user accounts thx to `make dbseed` .

- Open the Prisma [admin panel](http://localhost:4466/renty/dev/_admin) (or if you allready are a GraphQL veteran, the Prisma [Graphql Playground](http://localhost:4466/renty/dev)).

- You'll surely be faced with a terrible red warning telling you that you don't have sufficient permission to view this page but don't worry .

- Go back to your terminal and enter `prisma token -e .dev.env` .

- Copy the generated JWT string and on the admin panel webpage, click on the cog wheel in the top right corner then paste the string on the *Secret Token* field.

- If you don't see the tables names on the left panel, send a quick message to our [teams channel](https://teams.microsoft.com/_#/conversations/19:4b366d04ce67464b81de66d97ba7e389@thread.v2?ctx=chat). But if you do, enjoy, you just sucessfully deploy a big part of the Renty stack in dev stage 👍.

## Graphql_server API

A dedicated [README](./graphql_server/README.md) file will guide you to get it up.

## Scrapper

U Allready know what to [read](./scrapper/README.md) here

## Seed

Nothing too fancy, just have a look at the [main.go](./seed/main.go) file to get what's done here.

# ⚠️ Important

Please ensure that your change to [datamodels](./datamodels) files wont break the dependent microservices above.

FR: Veuillez vous assurer avant chaque push que vos changements sur le datamodel ne provoquent pas 
de régressions dans les webservices ci-dessus.