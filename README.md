# 🏘️ Renty - _Your personnal real estate_
[![pipeline status](https://gitlab.com/renty-dev/renty_src/badges/master/pipeline.svg)](https://gitlab.com/renty-dev/renty_src/-/commits/master)
[![coverage report](https://gitlab.com/renty-dev/renty_src/badges/master/coverage.svg)](https://gitlab.com/renty-dev/renty_src/-/commits/master)*[![Website renty-grafana.herokuapp.com](https://img.shields.io/badge/monitoring-grafana-yellow)](https://renty-grafana.herokuapp.com) [![Website therentyapp.com](https://img.shields.io/website-up-down-green-red/https/therentyapp.com.svg)](https://therentyapp.com/)

    If you don’t own a home, buy one. If you own a home, buy another one. If you own two homes, buy a third. And, lend your relatives the money to buy a home.
    John Paulson, investor and multi-billionaire

## Introduction :

The Renty project aim to be a complete digitalisation platform for all real estate rental operations. If you are new here, you may want to check the [wiki](https://gitlab.com/renty-dev/renty_src/-/wikis/home#renty-developers-wiki) first 😊. As for now, all documentations efforts are still work in progress. Do not hesitate to yell at the mainteners for everything that may be missing or confusing 🦑.

### I. Backend

#### A. Datamodel and Prisma server

  1. Datamodels

  Written in the prisma Graphql [Schema Definition Langage](https://www.prisma.io/blog/graphql-sdl-schema-definition-language-6755bcb9ce51), this schemes define the global data and the actual postgresql database architecture 🐘.  

  2. Prisma server

  Entirely generated from the [datamodels](./backend/datamodels) files, the prisma server block, (see the [docker-compose](./backend/docker-compose.yml) file) is offering us an entire graphql API
  handling all off our database operations.

  3. Codegen / the prisma clients

  Each webservice in the backend directory is built upon the generated prisma go client, the [graphq_server](./backend/graphql_server/README.md) is ensuring the major tasks like user authentication,
  registration, user offer creation etc.. 
  In future releases (see issue #9), The [ner](./backend/ner/README.md) python programm (WIP) will be helping the [scrapper](./backend/scrapper/README.md) to get all informations from scrapped rent offers websites.

#### B. Schemes and Graphql API
  
  1. GraphQL Schema directory and gqlgen

  2. Directive(s) and Resolver(s)


#### C. Webservices


  1. The GraphQL API

  👀 See the dedicated [README](./backend/graphql_server/README.md)

  2. Named Entity Recognition service

  (Will be) written in python with the help of the [Spacy](https://spacy.io/api/entityrecognizer/) library, the goal of this tool is to tokenize the rent offers descriptions to extract usefull entities like LOCATION, REALESTATE_INFOS, AREA ... etc

  3. Web scrapper service

  Another Golang tool who scrap datas from real estate offer websites (currently only Leboncoin) to enrich the renty real estate offers database.
  
#### D. Backoffice

   1. VueJS

   ...
   
   2. Apollo

   ...


### II. Webapp FrontEnd :

  1. Svelte JS
  
  ...

  2. Sapper
  
  ...

### III. Mobile :

  1. Flutter
  
  ...


## Just show me what you've been working on :

| Service      | Stage     |          URL                                    |
|:-------------|:---------:|:-----------------------------------------------:|
| 🦄 Webapp    | prod    📀| [therentyapp.com](https://therentyapp.com)      |
| 🦄 Webapp    | preprod 💿 | [therentyapp.xyz](https://therentyapp.xyz)      |
| 🦄 Webapp    | dev 💾    | [renty-frontend-dev.herokuapp.com](https://renty-frontend-dev.herokuapp.com)      |
| 📲 MobileAPK | preprod 💿 | [renty-mobile-dev.herokuapp.com](https://renty-mobile-dev.herokuapp.com/android/) |
| 🧠 GraphQLAPI| preprod 💿 | [renty-gqlserver-dev.herokuapp.com](https://renty-gqlserver-dev.herokuapp.com)    |
| 👮 Backoffice| preprod 💿 | [backoffice.therentyapp.xyz](https://backoffice.therentyapp.xyz)                  |
| 👮 Backoffice| dev 💾    | [renty-backoffice-dev.herokuapp.com](https://renty-backoffice-dev.herokuapp.com)   |


\* The coverage value presented here is not the global coverage of the project, it's only the last value in backend/graphql_server tests in CI